---
title: Bibliothèque
date: 2020-12-20T09:01:57.000+00:00

---
## Kendo

### En français

#### « Kendo – Vers l’Uchi parfait » - Jean-Paul BELHOMME, Winson Press Singapour, 2014

![](/images/2022-06-01_17-23.png)

Résumé : Découvrez : la préparation corporelle et mentale indispensable à l'attaque, l'analyse de l'adversaire, la réalisation de l'Ushi et des méthodes d'entrainement éprouvées. Enseignant de Kendo (BFHEK) et de Iaïdo (BFEI), l'auteur souligne l'influence de l'esprit sur la qualité de la technique. Unique en son genre, cet ouvrage s'adresse aux pratiquants de tous niveaux. Docteur en pharmacie mais aussi producteur d'un café d'exception, Jean-Paul Belhomme, 5ème dan de Kendo et de Iaïdo, transmet au lecteur sa passion du sabre et les moyens d'accéder à un Kendo de qualité.

#### « Kendo – La voie du sabre, une école de vie » - Minori ENDO, Philippe LABAYE et Jean-Paul BELHOMME, Wagram Editions 2021

![](/images/2022-06-01_17-24.png)

Résumé : Cet ouvrage consacré au Kendo répond aux nombreuses questions que se posent tout pratiquant, qu’il soit débutant ou confirmé. Sa présentation pédagogique permet de retrouver facilement des informations ou définitions spécifiques mais aussi d’élaborer des cours sur des thèmes choisis.

Les co-auteurs ont voulu transmettre les enseignements de nombreux professeurs japonais de renom, portant le grade exceptionnel de 8ème dan Hanshi de Kendo. Leur travail synthétise plus de 30 années de pratique et d’analyse destinées au plus grand nombre. Le lexique permet de retrouver facilement les définitions et les articles se rapportant à un thème donné.

Endo Minori, 6ème dan Renshi de Kendo a su transmettre toute la richesse de la culture japonaise directement liée à « la voie du sabre ». Philippe Labaye, 7ème dan Kyoshi de Kendo nous fait bénéficier de sa longue et riche experience de compétiteur et d’enseignant. Jean-Paul Belhomme, 5ème dan de Kendo et de Iaïdo, comme dans son ouvrage « Vers l’Uchi parfait », partage l’essentiel des fondamentaux du Kendo. Des dessins précis enrichissent le texte. Dans le même but, les liens et des adresses de site Web constituent un atout supplémentaire à la pédagogie de l’ouvrage. Approfondir l’essence mêm du Kendo, préparer un passage de grade, une compétition ou simplement perfectionner sa technique sont des objectifs facilités par la mise en pratique des conseils distillés au fil de cet ouvrage.

#### « Mémoires de Kenichi Yoshimura » - Kenichi YOSHIMURA, Edition CNKDR – FFJDA, 2020

![](/images/2022-06-01_17-25.png)

“J’écris ces mémoires à l’occasion de l’anniversaire de mes 50 ans en France. Depuis très longtemps déjà en vue de cet anniversaire, je couvais le projet de rédiger un document qui servirait les pratiquants français, une sorte de  
référence du kendo en général. Je considérais que cette tâche serait ma dernière mission à accomplir et aussi, qu’elle serait l’épilogue qui marquerait mon long parcours en France.” _KENICHI YOSHIMURA_

Kenichi YOSHIMURA est japonais. Arrivé en France en 1970 (il était alors 4ème dan de Kendo), il s'est très vite intéressé au Kendo français et a été l'un des acteurs essentiels de son développement pendant 30 ans. Aujourd'hui 8ème dan Kyoshi de Kendo, 6ème dan de Iaïdo, il nous porte un ouvrage relatant ses 50 années passées en France et comment selon lui, doit-on pratiquer le Kendo.

#### « Budoscope Tome 10 – Découvrir le Kendo » - C. HAMOT et K. YOSHIMURA, Editions Amphora 1991

![](/images/2022-06-01_17-26.png)

Résumé : Découvrir le KENDO. En 25 photos et en 166 dessins, ce livre vous fait pénétrer dans la " voie du sabre " et vous donne les éléments essentiels pour pratiquer cet art de combat qui se présente également sous l'aspect d'un grand sport international d'escrime au shinai. Elaboré à la fin du XIXe siècle à partir des techniques au sabre du KEN JUTSU, lui-même héritier de plus d'un millénaire d'expérience sur les champs de bataille de l'ancien Japon, le KENDO, après la Seconde Guerre mondiale, a peu à peu défini sa voie. Discipline propre à former le corps et le cœur de l'être humain, le KENDO, par le respect d'autrui qu'il impose, veut contribuer à une meilleure harmonie sociale, et en fin de compte, à la Paix.

#### « Dojo – Le temple du sabre » - Pierre DELORME, Editions de l’Eveil, 2007

![](/images/2022-06-01_17-28.png)

Résumé : " Mennn... ! " Le cri qui ponctue l'attaque à la tête. Le professeur s'interrompait alors quelques instants. " Ah! c'est encore le jeune Okada... Ça y est, il s'est endormi. Bien ! Continuons le cours. " Le professeur, habitué, reprenait alors tranquillement la leçon pendant que le meilleur élève... en kendo de l'Université rêvait à d'autres combats victorieux. Ce récit autobiographique raconte l'histoire d'un jeune homme, passionné par l'étude du sabre, découvrant au Japon l'atmosphère des dojos traditionnels, la rudesse du comportement samouraï et surtout l'exceptionnelle figure d'un maître. Par ses annotations sur le quotidien de la pratique et de la vie au Japon, ce livre attachant ravira tous ceux qui se reconnaissent dans cet " appel oriental ", non seulement kendokas, mais aussi judokas, karatékas, aikidokas ou même simplement amoureux de l'esprit japonais.

### En anglais

#### « Kendo Kata : Essence and applications » - Yoshihiko INOUE, Kendo World Publications, 2003

![](/images/2022-06-01_17-30.png)

Résumé : Le Coeur de cet ouvrage est la philosophie qui se cache derrière ces katas de la All Japan Kendo Federation Kendo. Inoue Sensei se penche attentivement sur l’histoire de la formation de ces kata en utilisant des enregistrements pris à l’époque et passe à la loupe les fondement théoriques de chaque kata. Les explications fournies pour tous les katas, de 1 à 10, sont accompagnées de nombreuses aidant à la compréhension de chacun. Une ressource importante pour tout pratiquant de kendo, du débutant au confirmé.

#### « Kendo – Culture of the sword » - Alexander BENNETT, University of California Press, 2015

![](/images/2022-06-01_17-32.png)

Résumé : Ce livre est le premier récit historique, culturel et politique approfondi en anglais traitant de cet art martial japonais de l'escrime, depuis ses débuts dans l'entraînement militaire et les écoles médiévales anciennes jusqu'à sa pratique généralisée en tant que sport mondial aujourd'hui. Alexander Bennett montre comment le Kendo a évolué à travers un processus récurrent de « tradition innovante », qui a servi les idéologies et les besoins changeants des guerriers et des gouvernements japonais au cours de l'Histoire. Le Kendo suit le développement de l'escrime japonaise depuis les prétentions aristocratiques et esthétiques des guerriers médiévaux de la période Muromachi jusqu'au patriotisme nostalgique de l'État Meiji en passant par l'élitisme samouraï du régime d'Edo. Le Kendo a ensuite été influencé dans les années 1930 et 1940 par un gouvernement d'après-guerre qui cherchait à raviver l'estime de la culture traditionnelle parmi la jeunesse japonaise pour acquérir une reconnaissance internationale comme outil de _Soft Power_. Aujourd'hui, le Kendo devient de plus en plus populaire à l'échelle internationale. Mais alors même que de nouvelles organisations et de nouveaux clubs se forment dans le monde, l'exclusivité culturelle continue de jouer un rôle dans l'évolution continue du Kendo, car le sport reste étroitement lié au sentiment d'identité collective du Japon.

#### « The Official Guide for Kendo Instruction » - All Japan Kendo Federation, second edition, 2020

![](/images/2022-06-01_17-33.png)

« The Official Guide for Kendo Instruction » - All Japan Kendo Federation, second edition, 2020

## Le sabre en général

### En français

#### « Le livre des 5 roues » - Miyamoto Musashi, Budo Editions 2014

![](/images/2022-06-01_17-34.png)

Résumé : Devenu un personnage légendaire, la vie aventureuse et les exploits de Miyamoto Musashi ont inspiré d’innombrables romans, nouvelles et pièces de théâtre. À l’âge de soixante ans, quelques mois avant sa mort, il se retire dans une grotte pour méditer et rédige à l’intention de ses disciples l’œuvre majeure de sa vie : Le Livre des Cinq Roues.  
 Le Livre des Cinq Roues porte sur les arts martiaux et plus particulièrement l’escrime. Mais les principes qu’il énonce trouvent aussi à s’appliquer à toutes les activités de nature stratégique, à tous les gestes de la vie quotidienne. Cet ouvrage n’est donc pas seulement un livre de stratégie guerrière ou pour l’action. C’est aussi un guide sur la Voie, qui énonce les principes d’un art de vivre. Livre à la fois d’action et de sagesse, il révèle le secret d’une stratégie victorieuse, d’un trajet initiatique qui passe par la maîtrise de soi.

#### « Musashi – Le Samouraï solitaire » - William Scott WILSON, Budo Edition 2008

![](/images/2022-06-01_17-37.png)

Résumé : À l’âge de treize ans, Miyamoto Musashi tuait son premier adversaire et entamait ce qui allait devenir une longue série de duels légendaires. À l’aube de ses trente ans, il avait pris part à plus de soixante affrontements en ne déplorant aucune défaite. Au cours des trente années suivantes, il n’enleva plus la vie à quiconque et se contentait désormais d’éprouver ses talents en contrariant les offensives répétées de ses adversaires jusqu’à ce qu’ils reconnaissent son incontestable supériorité. C’est à cette période que le maître de sabre commença à élargir ses horizons et explorer les voies du bouddhisme zen. Musashi allait devenir une légende de son vivant et exposer sa philosophie ainsi que sa pénétration dans le domaine de la stratégie dans son célèbre traité Gorin-no-sho (Le Livre des cinq roues), œuvre qu’il rédigea au crépuscule de sa vie. À partir d’authentiques sources japonaises, l’auteur, William Scott Wilson, brosse un portrait inoubliable de cet illustre personnage historique. Musashi, le samurai solitaire est la première biographie jamais publiée en français, consacrée à ce maître de sabre et chercheur du XVIIe siècle aux multiples facettes et à la personnalité complexe, et dont l’héritage éprouve le temps et l’espace.

#### « Le Sabre de Vie » - Munenori YAGYU, Budo Editions 2018

![](/images/2022-06-01_17-38.png)

Résumé : Le sabre de vie, Heiho Kadensho en japonais, est un des trois grands classiques de l'escrime japonaise, au côté du Livre des cinq roues (Gorin-no-sho) de Miyamoto Musashi et de L'esprit indomptable (Fudoshi-shinmyo-roku et Taiaki) de Takuan soho. Alors que Takuan Soho, en tant que moine, met l'accent sur l'aspect spirituel de l'escrime et que Miyamoto Musashi, escrimeur renommé, s'attache au côté pragmatique, Yagyû Munenori emprunte la ligne étroite qui sépare ces deux extrêmes en proposant à la fois une approche philosophique et une approche pratique l'illustrant d'exemples concrets.  
 Le sabre de vie est pour celui qui s'intéresse à l'esprit du samouraï une source de connaissances indispensable.

#### « L’esprit indomptable » - Soho TAKUAN, Budo Editions 2007

![](/images/l-esprit-indomptable.jpg)

Résumé : Dans leur ensemble, les trois écrits qui constituent cet ouvrage s'adressent à la classe des samouraïs, et ont pour objet d'unifier l'esprit du zen à l'esprit du sabre. Le conseil donné mêle les aspects pratiques, techniques et philosophiques qui président à toute confrontation et apportent à l'individu la connaissancede soi, qui deviendra à terme un art de vivre. En établissant clairement l'unité du zen et du sabre, Takuan a influencé les écrits de grands maîtres qui continuent à être lus et leurs préceptes à être appliqués ; c'est le cas du Heiho Kadensho de Yagyu Munenori et surtout du Gorin no Sho (Le Livre des Cinq roues) de Miyamoto Musashi. Le style de ces hommes varie, mais leurs conclusions s'imprègnent d'un haut niveau d'introspection et de compréhension, qu'elles soient exprimées en termes de " liberté et de spontanéité " par Musashi, ou " d'esprit ordinaire qui ne connaît aucune règle " par Munenori ou encore " d'esprit indomptable " par Takuan.

#### « Sabres japonais d’exception » - Yoshindo YOSHIHARA, Nuinui Editions 2018

![](/images/2022-06-01_17-40.png)

Résumé : Cet ouvrage présente le grand maître des arts japonais Yoshindo Yoshihara, élevé au rang de "Légende vivante des traditions japonaises" par le gouvernement de Tokyo, bien connu des passionnés des arts japonais et de sabres traditionnels, et choisi pour la séance de fabrication des sabres traditionnels dans le film "Kill Bill 2'.

Ce livre grand format merveilleusement illustré traite en détail du complexe processus de fabrication du sabre japonais – forge, trempe, polissage... – et va certainement jeter les fondements d'une compréhension plus approfondie et complète de l'art du sabre japonais.

#### « Nippon To » - Serge DEGORE, Editions du Portail 2007

![](/images/2022-06-01_17-41.png)

Résumé : Véritable symbole du pays où il est né, le sabre japonais incarne à lui seul le monde fascinant des Samurais. Ecrit par un spécialiste des arts martiaux, cet ouvrage, de 144 pages et 400 illustrations, guide l'amateur dans l'expertise difficile des armes japonaises. La forme des sabres, les marquages, les différentes écoles d'armuriers sont les renseignements indispensables pour connaître les armes blanches des Samurais. Une étude complète et pratique sur les sabres des guerriers de l'ancien Japon.