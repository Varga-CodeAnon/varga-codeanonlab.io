---
title: Inscription
date: 2020-12-20 07:01:57
---

Les inscriptions se font directement **au dojo** et sont possibles à la suite d'un cours d'essai.

Les tarifs sont les suivants :

|                                     | Enfants 10-12 ans |
| :---------------------------------: | :---------------: |
|          Forfait annuel :           |       225€        |
|         Trimestre d'essai :         |       130€        |
| Adhésion à l'association de kendo : |        45€        |

<br />

|                                     | Adolescents 12-16ans |
| :---------------------------------: | :------------------: |
|          Forfait annuel :           |         260€         |
|         Trimestre d'essai :         |         160€         |
| Adhésion à l'association de kendo : |         45€          |

<br />

|                                     |              Adultes              |
| :---------------------------------: | :-------------------------------: |
|          Forfait annuel :           | 380€ (305€ étudiants ou chômeurs) |
|         Trimestre d'essai :         |               210€                |
|        Carnet de dix cours :        |               160€                |
| Adhésion à l'association de kendo : |                45€                |

<br />

> Notes :
> - Les tarifs permettent aussi la pratique de l'[escrime japonaise](http://www.voiesdeshommes.com/escrime-japonaise/) (battodo, iaido, kenjutsu) et de la [sophrologie](http://www.voiesdeshommes.com/sophrologie/). Une adhésion à l'association d'escrime japonaise de 55€ sera alors demandée.
> - Paiement fractionné possible
> - Tarif famille / Tarifs deux disciplines
> - Situation particulière : nous consulter
