---
title: Nito
categories: ''
tags:
- Articles
top: false

---
_Article écrit par Georges Mc Call en Janvier 2017 et publié sur son site_ [_kenshi247.net_](https://kenshi247.net/blog/2017/01/03/nito-ryu-kendo-a-brief-discussion/)

_Traduction française : **Fabien Rivaille** Avril 2022_

***

Le débat sur le kendo nito-ryu est quelque chose que j’ai délibérément évité ces dernières années, mais le décès du kenshi nito-ryu le plus réputé dans le pays à la fin du mois de décembre, Toda Tadao hanshi, m’a fait penser qu’il était temps de m’attaquer au sujet… du moins très brièvement. Pour une discussion plus détaillée sur le sujet, vous devez vous asseoir avec moi dans un pub !

Premièrement, voici la photo très populaire que j’ai uploadée en décembre sur Facebook afin de rendre hommage à Toda sensei. Je l’ai prise le 5 mai 2009 :

![](/images/2022-06-01_19-13.png)

A présent, je vais vous donner un bref survol du contexte et de la culture du kendo nito-ryu d’un point de vue historique, suivi de commentaires personnels.

### Nito-ryu avant l’apparition du shinai

Il existe plusieurs _koryu_ qui incluent l’utilisation de deux sabres. La plus connue est bien évidemment le _Niten-ichi-ryu_, le style que l’on attribue au sabreur le plus décrit dans la littérature et le cinéma au Japon : Miyamoto Musashi.

Parmi les autres écoles qui intègrent la pratique à deux sabres, il y a les écoles _Yagyu shinkage- ryu_, _Shingyoto-ryu_, et _Katori shinto-ryu_. Il est important de noter que les kata à deux sabres, même lorsqu’ils existent, ne constituent qu’une très petite partie d’une plus grande série de kata.

### Nito-ryu après l’apparition du shinai

Les prototypes de _shinai_ et _bogu_ actuels furent développés et expérimentés pendant plusieurs années, à partir du milieu du 18ème siècle jusqu’au tout début du 20ème siècle, quand la forme fut alors considérée comme aboutie. Les deux écoles mentionnées le plus souvent à ce stade — _Jikishinkage-ryu_ et _Hokushin Itto-ryu_ — ne possèdent aucune part de _nito_. Toutefois, on peut émettre l’hypothèse que des personnes pourraient avoir essayé de prendre deux _shinais_ pour s’affronter, car après tout cela pouvait paraître amusant !

### Gekken kogyo

C’est probablement dans la deuxième moitié du 19ème siècle, avec l’introduction du spectacle payant et éphémère nommé _Gekken kogyo_ que le _nito_ a fait son entrée en scène pour la première fois. Ces représentations incluaient une grande variété dans les combinaisons d’armes utilisées et voyaient également la présence de combattantes.

Après que le kendo au _shinai_ soit finalement introduit comme une pratique de la nouvelle force de police de Tokyo (_Keishicho_), tous les représentants les plus doués y ont trouvé un emploi et le _Gekken kogyo_, devenant l’ombre de ce qu’il fut, disparut finalement.

### La formalisation du kendo : Busen et Koshi

La première tentative de standardisation du kendo a eu lieu dans deux centres principaux : le _Dai Nippon Butokukai_ (à l’origine un centre d’entraînement le _Bujutsu Kyoin Yoseijo_ est finalement devenu le _Budo Senmon Gakko,_ ou « _Busen_ » ) et le _Tokyo Koto Shihan Gakko_ (« _Koshi_ » ). Les deux professeurs les plus influents étaient Naito Takaharu et Takano Sasaburo. Aucun de ces deux professeurs n’a ni pratiqué, enseigné ni réellement discuté du kendo _nito-ryu_, ce qui bien sûr a influencé leurs étudiants et les centres d’entraînements que ceux-ci ont développés.

Nishikubo Hiromichi, kenshi du _Muto-ryu,_ devint le professeur en chef du _Busen_ en 1919. Dans les cercles d’arts martiaux, il est connu pour être celui celui qui a poussé la rebaptisation du « _jutsu_ » en

« _do_ » mais il exerce également une autre forte influence sur le _Busen_ : il déconsidère l’attaque à une main, se plaignant que ces techniques étaient « irréalistes » et « faibles ». C’était certainement le point de vue de Naito également (bien que l’élève favori de Naito, également professeur au _Busen_, Miyazaki

Mosaburo, était renommé pour son _katate-men waza_ très puissant). Quand Naito tomba malade et décéda, Ogawa Kinnosuke (que Nishikubo appréciait beaucoup) devint le professeur principal du _Busen_ jusqu’au milieu de la Seconde Guerre Mondiale. Dans ce contexte, il est facile de comprendre pourquoi le _Busen_ n’a jamais produit de pratiquant de _jodan_, et ne parlons même pas du _nito_.

Ayant dit cela, il est important de noter que l’un des premiers et plus anciens membres du _Butokukai_, Mihashi Kanichiro, était un maître renommé dans la pratique du _nito-ryu_. Etudiant de Momoi Junzo, il était l’un des pratiquants repérés lors des Gekken Kogyo pour devenir enseignant de kendo au _Keishicho_. En 1899, il devint un professeur de kendo au _Butokukai_ et fut le tout premier à recevoir le titre de

« _hanshi_ » en 1903. Un autre pratiquant renommé de _nito-ryu_, Okumura Torakichi (fils d’un autre maître de _nito-ryu_ Okumura Sakonta), s’est entraîné sous la direction de Mihashi de 1900 jusqu’à sa mort en 1909. Torakichi était à la fois le successeur du « _Okumura nito-ryu_ » de son père et du « _Musashi-ryu_ » de Mihashi. Ces deux styles étaient de nouvelles inventions basées sur l’expérience et non sur une transmission du passé.

En ce qui concerne le _Koshi_, alors que Takano obligeait tous les étudiants de kendo à apprendre le _Jodan_ comme partie intégrante de l’enseignement, le _nito_ semblait être largement ignoré.

### Le kendo Nito-ryu apparaît

Le kendo de compétition n’était pas existante jusque dans les années 1920 et même à cette époque, il constituait un événement rare. Le kendo en tant que matière scolaire était alors présent depuis quelques années et commencçait tout juste à gagner en popularité dans les universités. Etant jeunes, les étudiants apprécient l’émotion de la compétition. Une fois plus âgés, les professeurs plus chevronnés ont continué de désapprouver le shiai. Le _Busen,_ en particulier, ne faisait pas d’entraînement au shiai et ne participait pas au shiai jusqu’à la mort de Naito en 1929 (les élèves pratiquaient quelque fois secrètement le shiai, sans que leurs sensei en aient connaissance !). La compétition à cette époque était généralement pratiquée par les étudiants en université et les personnes proches de ces cercles. C’est là que l’on vit apparaître pour la première fois le _nito_.

La grande majorité des compétitions d’avant la Seconde Guerre Mondiale se déroulaient en équipe sur le principe du « _kachinuki_ » , un style où, si l’on gagne, on continue d’affronter la personne suivant de l’équipe adverse. Une particularité de ce type de _shiai_ est qu’une égalité entraîne la sortie des deux compétiteurs et l’entrée du compétiteur suivant de chaque équipe. C’est là que le _nito_ a trouvé une utilité : si l’équipe adverse possédait un compétiteur très fort, vous pouviez utiliser un _nito_ pour provoquer une égalité (plus facile à obtenir du fait d’être d’’une nature plus défensive) et ainsi faire sortir ce _kenshi_. En lisant les anecdotes sur le sujet, on peut voir que cette stratégie était particulièrement répandue à cette époque, à tel point que certaines compétitions universitaires bannirent l’utilisation du _nito_.

Mais d’où pouvaient bien provenir ces kenshi pratiquant le _nito-ryu_ ? Qui avait bien pu le leur enseigner ? Je pourrais bien émettre une hypothèse sur ces questions, mais avant tout je vous demanderai de relire l’article à propos de Fujimoto Kaoru, que j’avais publié en 2009.

Alors que c’est assez évident, ma supposition est qu’à cette époque, de manière similaire à Fujimoto, la grande majorité des kenshis _nito-ryu_ venaient de la population jeune des universités (en opposition aux kenshis professionnels) et étaient auto-didactes (surprise). Dans les milieux professionnels du kendo, le _nito-ryu_ était inexistant.

Le succès de deux kenshis _nito-ryu_ Fujimoto et Kayaba Teruo durant le _Showa Tenran-jiai_ (1934 et 1940) suggère que le kendo _nito-ryu_ était peut-être plus populaire qu’il ne semblait l’être. Alors qu’il ne fait aucun doute que le succès de Fujimoto en 1934 en ai pu inspirer d’autres, en consultant beaucoup de sources des années 30, il est aisé de montrer que le _nito-ryu_ était — en dehors du _kachinuki shiai_ du niveau universitaire — une simple réflexion. Cela ne concernait pas le kenshi sérieux. Et de toute façon, alors que le Japon est entré en guerre dans les années 30, le kendo lui-même a changé pour devenir plus

« réaliste », ce qui, il est inutile de le préciser, n’incluait pas l’utilisation simultanée de deux sabres.

### L’après-guerre

Alors que le Kendo retrouvait un nouvel élan après la Seconde Guerre Mondiale, la All Japan Kendo Association fraichement créée décida de bannir complètement le _nito-ryu_ des compétitions scolaires et universitaires (il ne faisait même plus partie du _shinai kyogi_). Ce bannissement fut maintenu jusqu’en 1991 et entraîna l’éradication quasi totale du kendo _nito-ryu_ au Japon. Bien évidemment, certains adultes continuèrent à le pratiquer durant cette période et même un petite poignée de pratiquants très doués combattit durant les All Japan Kendo Championships. Ces personnes étaient, comme vous vous en doutez, auto-didactes.

### L’état actuel : une mini-renaissance ?

Au cours de ces dernières années, j’ai vu le kendo _nito-ryu_ exploser. Cette explosion semble venir plus de l’étranger que du Japon, mais il y a certainement plus de personnes pratiquant le _nito-ryu_ de nos jours qu’à l’époque de mon arrivée au Japon. Qu’y a-t-il derrière cette explosion ?

1. - Musahi-kai : Pour la première fois dans l’histoire du kendo nous avons un groupe qui pratique et, le plus important, enseigne le _nito-ryu_ de façon systématique. Ce groupe a gagné en popularité au début des années 2000 en tant que dojo semi-commercial en ligne répondant aux besoins des individus au Japon, mais a grandi pour devenir une organisation plus étendue avec un ensemble de groupes interconnectés et même des étudiants à l’étranger.

2. - Mise en lumière : En 2007, pour la première fois depuis presque 40 ans, le kenshi Yamana Nobuyuki de Tokushima pratiqua nito-ryu au cours des All Japan Kendo Championships. Cela a provoqué un grand nombre de débats (positifs) et de discussions au Japon autour du _nito-ryu._ Yamana Nobuyuki joua également un rôle important de modèle pour les kenshi _nito_ plus jeunes et débutants, ce qui n’est pas rien à mon sens.

3. - Université : La levée du bannissement du _nito_ au niveau universitaire dans les shiai a rendu plus facile la capacité de s’approprier le style par les étudiants mais il semble, au moins pour le moment, que peu d’entre eux s’y intéresse. Avec la combinaison des 2 premiers points évoqués, il semble que de nos jours l’intérêt commence à grandir et vous pouvez commencer à voir régulièrement des pratiquants _nito_ dans les compétitions. Peut-être que le sensei en chef _nito-ryu_ du futur, dirigé par le sensei du _Musashi-kai_, proviendra de cette génération.

4. - Le livre de la All Japan Kendo Association (ZNKR) : Il n’y a rien de mieux pour officialiser quelque chose que de l’inclure dans un livre et c’est ce que fit la ZNKR en publiant leurs propre règles et standards. Bien qu’il ne supprime pas complètement la marque de déshonneur du choix de pratiquer le _nito-ryu_, il donne au moins une lueur d’acceptabilité.

5. - Intérêt des pratiquants non Japonais : J’ai laissé ce point en dernier mais il constitue un des points les plus intéressants dans la discussion quand on parle de _nito-ryu_. C’est aussi un sujet que j’aimerais aborder en détail dans un article futur … ou alors vous pouvez me payer un bière !

### Conclusion

Veuillez noter que cet article n’est pas un guide complet, mais plutôt un aperçu rapide du _nito-ryu_, en particulier de son histoire, et une discussion courte sur sa récente popularité selon mon propre point de vue. Je ne suis pas qualifié pour discuter des aspects techniques, mais son histoire est assez simplement expliquable.

Pour résumer :

1. en dehors d’un petit boom parmi les étudiants universitaires dans les années 20 et 30, le kendo _nito-ryu_ existait (à peine) en marge du kendo jusqu’à très récemment. Certains pourraient argumenter sur le fait qu’il s’agit toujours d’une activité marginale, mais on ne peut nier sa popularité accrue sur ces 10 dernières années.
2. avant l’extension du groupe Musashi-kai dans les dernières années, les pratiquants étaient peu nombreux et éloignés les uns des autres, et quasiment tout le temps auto-didactes.

Et, un dernier point : tout en haut de cet article, j'ai déclaré qu'une _« discussion sérieuse sur le nito-ryu kendo est quelque chose que j'ai délibérément évité au cours des dernières années »._ La raison n'est pas due à un manque d'intérêt, mais parce que je soupçonnais que je recevrais des plaintes de pratiquants de nito-ryu kendo qui ne comprendraient pas mon analyse. Le fait est que, d'un point de vue historique, les choses sont vraiment aussi simples que celles décrites ci-dessus.

Personnellement, je suis heureux que le nito-ryu kendo devienne plus organisé, moins aléatoire qu'il ne l'a été par le passé. Cela ajoute quelque chose d'intéressant au mélange et je suis heureux de m'engager dans le keiko avec mes amis nito-ryu chaque fois que je le peux, car cela m'aide dans mon propre shugyo.