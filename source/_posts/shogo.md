---
title: Une Brève Enquête Sur Le Système SHOGO
categories: ''
tags:
- Articles
top: false

---
Article écrit par Georges Mc Call en Janvier 2010 et publié sur son site kenshi247.net

_Traduction française : **Fabien Rivaille** Avril 2022_

SHOGO (称号) en japonais se traduit simplement par « titre » ou « rang » et le mot peut être utilisé dans de nombreux domaines, par exemple les titres formels de noblesse, les grades militaires, les grades universitaires etc.. et de manière générale, dans le monde du sport, entre amis, etc...

L'usage du mot qui va m’intéresser ici est bien sûr celui qui est en rapport avec le monde du budo et plus précisément l'usage qui en est fait par le Dai Nippon Butokukai (1895-1946) et qui perdure aujourd'hui par le biais de son héritier spirituel, le Zen Nippon Kendo Renmei (Fédération panjaponaise de kendo). Veuillez noter que les informations sur le Butokukai présentées ici concernent le Kendo (diversement appelé gekken, gekiken et/ou kenjutsu) mais que, finalement, tous les arts martiaux du Butokukai ont fini par utiliser le même système (kyudo, jukendo etc…).

Bien que cet article soit censé se concentrer sur les titres de shogo, je ne pouvais ignorer le système DAN-I ( 段位) lors de mon enquête. Le système dan-i utilisé par le [Butokukai ](https://kenshi247.net/blog/2018/09/10/busen-and-koshi/)a été évidemment fortement influencé par le système utilisé dans le Judo, inventé par Kano Jigoro et utilisé au Kodokan. Entre autres choses, Kano était le directeur de [l'école normale supérieure de Tokyo ](https://kenshi247.net/blog/2018/09/10/busen-and-koshi/)entre 1893 et 1920 et [Takano Sasaburo ](https://kenshi247.net/blog/2011/05/25/takano-sasaburo-1862-1950/)a commencé à y travailler en tant que professeur de kendo en 1908. Kano était influent dans de nombreux cercles (y compris gouvernementaux) et je ne pense pas qu’il soit illogique de supposer qu'il a également eu une influence importante et directe sur la pensée de Takano à ce sujet. Takano est, bien sûr, devenu l’un des maîtres du kendo au Japon et un membre important du Butokukai.

Il convient également de mentionner (et certains détails sont inclus dans la liste ci-dessous) que le Keishicho (le service de la police métropolitaine de Tokyo) avait établi son propre système dan-i qui fonctionnait séparément de celui de Butokukai. Il y avait malgré tout une forte collaboration entre le Keishicho et le Butokukai (en fait, le gouvernement a émis un décret interdisant expressément au Keishicho de trop se mêler des affaires du Butokukai).

Quoi qu'il en soit, voici une échelle de temps des événements les plus importants qui menèrent au système que nous avons aujourd'hui. Les éléments présentant un intérêt particulier sont mis en évidence en gras.

À la fin, j'ai pris deux ou trois domaines clés et les ai développés un peu plus.

### **Événements importants dans la création et l'histoire du système shogo (et dan-i) rendus chronologiquement**

1878/79 : Création du [kenjutsu policier ](https://kenshi247.net/blog/2012/06/20/the-argument-for-the-revival-of-gekken/)et mise en place du Keishicho

1882 ou 83 : Premier système DAN-I (段位) utilisé par Kano Jigaro pour le remaniement du koryu Jujutsu en Judo. Il décerne les premiers grades de shodan à Shiro Saigo et Tsunejiro Tomita.

1886 : Le système kyu du Keishicho est défini (basé sur un système de classes (等) ou kyu (級) selon la source).

1893 : Kano devient le directeur de l'école normale supérieure de Tokyo

**1895 : Création du Butokukai (avril) et premières attributions du titre de SEIRENSHO (octobre).**

**1902 : Le Butokukai désigne les titres KYOSHI et HANSHI comme venant après celui de SEIRENSHO (ce système perdure jusqu'en 1934, date à laquelle SEIRENSHO devient RENSHI). L'âge minimum pour le HANSHI est fixé à 60 ans.**

**1903 : Onze grades KYOSHI/HANSHI sont décernés.**

1908 : Takano Sasaburo est nommé professeur de kendo à l'école normale supérieure de Tokyo en février et lance un système de classement DAN-I (段位) en juin.

1912 : Lorsque le Butokukai créa le [_Teikoku kendo no kata_](https://kenshi247.net/blog/2011/06/13/kendo-no-kata-creators/) , l'occasion fut saisie de discuter de l'uniformisation des systèmes de grades judo/kendo.

1913 : Bien qu'ils aient moins de 60 ans, les suivants se virent atttribués le titre HANSHI : [Naito ](https://kenshi247.net/blog/2012/12/04/naito-takaharu/)[Takaharu ](https://kenshi247.net/blog/2017/01/19/kensei-naito-takaharu/), Takano Sasaburo, Yamashita Yoshitsuga, Isogai Hajime, Nagaoka Hidekazu et Ichikawa Torashiro.

**1914 : Les règles d'attribution du SEIRENSHO sont fixées et la réglementation du kenjutsu/judo codifiée (le kenjutsu utilise « kyu », et le judo utilise « dan »).**

**1917 : Le Kenjutsu commence à utiliser le même système DAN-I (段位) que le judo. À cette période, le système ne va que jusqu'au rang de godan (5ème dan), après quoi les titres de SEIRENSHO, KYOSHI et HANSHI sont décernés.**

1919 : Le Butokukai change officiellement le nom de kenJUTSU en kenDO.

1920 : Kodokan annonce « une réglementation pour les grades de judo, en kyu et dan » 1923 : Le Keishicho crée son propre système de notation interne du budo.

1926 : L'école normale supérieure de Tokyo change le nom de gekken en kenDO. Un mois plus tard, le changement est officiel dans toutes les écoles du pays.

1930: L'école normale supérieure de Tokyo crée ses propres rangs de shogo - Tokushi, Shushi et Tasshi.

**1934 : Le titre de SEIRENSHO est remplacé par RENSHI.**

**1937: Les grades rokudan (6ème dan) et plus commencent à être obtenus à partir de cette année.**

1942 : Le Butokukai passe sous le contrôle du gouvernement militaire. Le titre KYOSHI change de nom et devient TASSHI.

1943 : Le Butokukai établit de nouvelles règles pour le système shogo. 1946 : Sous diverses pressions, le Butokukai est dissout.

**1953: Le Zen Nippon Kendo Renmei (ZNKR) est créé, adossé au Zen Nippon** [**Shinai Kyogi**](https://kenshi247.net/blog/2012/02/02/shinai-kyogi/) **Renmei (créé en 1950, ce dernier sera fusionné avec le ZNKR en 1954). À cette époque, le système DAN-I (段位) est prévu pour aller jusqu'à godan (5ème dan), suivi ensuite des titres shogo de RENSHI, KYOSHI et HANSHI.**

1957 : Par peur du déséquilibre de l’uniformisation qui existe avec le judo qui attribuait déjà des grades jusqu'au [judan](https://kenshi247.net/blog/2015/07/03/kendo-judan/) (10ème dan) , et bien que l'association soit différente, il a été décidé d’avancer la décision de 4 ans et de fixer immédiatement la limite des grades de kendo à judan (10ème dan).

**2000 : Le ZNKR décide qu'il n'y aura pas de nouveau judan (10ème dan) attribué et fixe la limite du système DAN-I (段位) à hachidan (8ème dan). HANSHI est déclaré être la plus haute distinction que vous puissiez recevoir. Il y a quelques kyudan (9ème dan) vivant au Japon.**

### 

Seirensho (精練証)

![](/images/2022-06-01_19-21.png)

Le prix SEIRENSHO est particulièrement intéressant. Celui-ci a été décerné pour la première fois lors du 1er [Kyoto Taikai ](https://kenshi247.net/blog/category/kyototaikai/)en octobre 1895 à 15 kenshi très expérimentés sur un total de 386 participants. Ce prix est le prédécesseur du titre RENSHI et continuera à être utilisé jusqu'en 1934, date à laquelle le titre RENSHI le remplace.

Puisqu'il s'agissait des premiers shogo décernés par le Butokukai (il faudra encore 8 ans avant que les titres de KYOSHI et HANSHI ne soient créés), il est difficile de deviner exactement la valeur du titre. Mon sentiment personnel est qu'il s'agissait d'un honneur exceptionnel décerné à des personnes ayant de grandes capacités et qui forçaient le respect au sein de la communauté Butokukai.

Les premières personnes à recevoir le prix étaient (nom, style) :

Ishiyama (Itto-ryu), Hagiwara (Jikishinkage-ryu), Hara (Tenji-ryu), Tokuno Sekishiro (Jikishinkage- ryu), Okamura Sakonta (Jikishinkage-ryu, shin nitto-ryu), Kagawa (Muto-ryu. Le plus grand gars dans la rangée arrière dans l'image ci-dessus), Yoshida (Seitoku taishi-ryu), Negishi Shigoro (Shinto mumen- ryu, professeur de Nakayama Hakudo. hanshi 1906), Umezaki (Shinkage-ryu), Matsuzaki Namishiro (Shinkage-ryu), Takayama Minesaburo (Jikishinkage-ryu), **Mamiya (Ono-ha itto-ryu. La photo ci- dessus est son Seirensho)** , Kominami (Muto-ryu), Abe (Jikishinkage-ryu), Mihashi (Musashi -ryu).

Pour que vous puissiez avoir une idée du nombre de SHOGO délivrés au cours des premières années, voici une échelle rapide du nombre attribué au cours des 6 premières années (toujours avant la délivrance de KYOSHI, HANSHI):

Année (nombre de participants au Butokusai / nombre de SEIRENSHO récompensés) :

1895 (386/15); 1896 (472/15); 1897 (482/6 dont Naito Takaharu); 1899 (766/8); 1900 (493/7); 1901 (825/8).

### KYOSHI, HANSHI (教士・範士)

En 1903, quatre titres KYOSHI et sept titres HANSHI furent décernés pour la première fois. Les tout premiers HANSHI étaient :

\[nom (style, préfecture, âge)\] Ishikawa (Itto-ryu, Kochi, 74)

Takao (Tecchu-ryu, Nagasaki, 73 ans) Shibue (Shinto munen-ryu, Nagasaki, 68 ans) Sakabe (Kyoshin mechi-ryu, Aiichi, 66 ans) Watanabe (Shinto munen-ryu, Tokyo, 66 ans) Mihashi (Musashi-ryu, Tokyo, 62 ans) Tokuno (Jikishinkage-ryu, Tokyo, 61 ans)

HANSHI est devenu le titre le plus élevé que vous puissiez obtenir au sein du Butokukai, avec une limite d'âge minimal de 60 ans (cette règle a été enfreinte en 1913 lorsque certains kenshi, y compris Naito Takaharu et Takano Sasaburo, se le vit décerné à un âge plus précoce. _NDLR : Cette règle n’est aujourd’hui plus appliquée_). De nos jours, le Shogo HANSHI est toujours le titre le plus élevé pouvant être obtenu en kendo.