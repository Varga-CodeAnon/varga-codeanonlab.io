---
title: Mokuso
categories: ''
tags:
- Articles
top: false

---
_Publié le 9 Juillet 2018 sur le_ [_Weblog de TOZANDO_](https://weblog.tozando.com/why-mokuso/)

_Traduction française : **Fabien Rivaille** Avril 2022_

### 黙想Mokuso 瞑想Meiso 正座 Seiza

Je suis sûr que beaucoup d’entre vous s’asseyent en silence et méditent les yeux fermés avant et après l’entraînement. Certains appellent cela Mokuso (pensée silencieuse), d’autres, Meiso (pensée les yeux fermés) ou encore Seiza (l’assise, tout simplement). Lorsque vous rejoignez le Dojo, l’enseignant vous expliquera sa signification et comment pratiquer cette méditation. Mokuso est presqu’aussi important que l’entraînement en lui-même.

La posture de base pendant la méditation consiste à s’asseoir avec vos genoux repliés. Ensuite, placez votre main droite devant votre nombril, paume vers le haut, puis placez votre main gauche sur votre main droite avec la paume vers le haut. Ensuite, fermez légèrement les yeux (ou laissez-les légèrement ouverts) et contrôlez votre respiration.

Si vous savez comment le Daibutsu (ndlr : Grand Bouddha) de Kamakura se tient, les mains devant lui, c’est facile à imaginer. C'est la posture de base pendant la méditation mais elle n'est pas forcément figée. La partie la plus importante de la méditation n'est pas votre posture mais votre esprit. Par la méditation, vous organisez vos pensées, vous concentrez votre esprit pour chercher au plus profond de votre cœur afin d’obtenir une nouvelle compréhension.

  
![](/images/2022-06-01_19-02.png "Grand Bouddha (Daibutsu) de Kamakura (Source : Wikipedia)")

Au cours de la pratique, plutôt que de simplement faire les mouvements chaque jour, vous pouvez voir un changement dans votre propre pratique et dans vos résultats si vous avez des objectifs mentaux spécifiques comme « aujourd'hui, je vais me concentrer sur le fait de m’améliorer dans ce domaine » ou « à la fin de la journée, je veux en arriver là ». Mokuso est le moment où vous visualisez une bonne image de ce que vous voulez améliorer avant de commencer à vous entraîner.

Les sportifs professionnels ou les athlètes olympiques incorporent presque toujours ce type d'entraînement mental dans lequel ils imaginent là où ils veulent être. Au Japon, on parle

« d’entraînement par l'image » où vous imaginez dans votre tête les mouvements que vous aimeriez faire, puis planifiez le chemin vers la réalisation de cette image. La méditation avant l'entraînement est aussi un type de cet « entraînement par l'image ».

Mokuso après la pratique est également conçu non seulement pour calmer votre cœur qui a eu des hauts et des bas et mais aussi pour réfléchir à votre pratique. C'est un moment pour réfléchir non seulement à la question de savoir si la pratique était bonne ou mauvaise, mais aussi à réfléchir sur vos propres actions et attitudes. Vous commencez à percevoir votre propre faiblesse lorsque vous réfléchissez à la raison pour laquelle vous ne pouvez pas garder votre cœur calme ou pourquoi votre esprit est instable. Ensuite, vous essayez d’appliquer ces réflexions lors de votre prochain entraînement lorsque vous pratiquez Mokuso avant l’entraînement. De cette façon, vous avancez étape après étape.

Ce type de méditation s'appelle Mokuso, ce qui est différent, bien que souvent confondu, avec Meiso. En quoi les deux sont-ils différents ? Mokuso implique de penser, méditer tandis que Meiso tente de parvenir à une détermination ou à une inconscience, une absence de réflexion. La similitude est que les deux impliquent une assise silencieuse, les yeux fermés, car sinon les caractéristiques sont totalement opposées.

Comme on l’a dit, dans certains Dojo, on appelle cela « Meiso ! » ou « Seiza ! ». Seiza signifie tout simplement s'asseoir en silence. Dans de nombreux cas, les mots utilisés au Dojo ne sont pas

nécessairement liés au sens strict du mot. La raison pour laquelle certains utilisent « Seiza » est que le Kendo s’est répandu à l’étranger et est pratiqué par des personnes de culture et de religion différente et que « Seiza » a une connotation moins religieuse que « Mokuso », pratiqué selon une tradition bouddhiste. Certains endroits utilisent « Meiso », mais la véritable intention de Meiso peut également avoir des effets significatifs pendant les entraînements et les matches, il se peut donc qu'ils adaptent délibérément « Meiso » plutôt que « Mokuso ».

Cela peut sembler être juste s'asseoir et fermer les yeux, mais quand vous vous engagez envers vous- même, comme « Je vais réussir aujourd'hui », cela vous aide à imaginer ce que vous voulez idéalement devenir. C'est un moment indispensable où vous rassemblez vos pensées lorsque vous entrez dans votre pratique. Beaucoup de gens ne font pas la distinction entre Mokuso et Meiso, mais si vous pratiquez les deux avec une juste compréhension de leurs différents objectifs, cela peut conduire à une meilleure qualité de pratique du Kendo.

### La respiration pendant le Mokuso

_(Source :_ [https://www.wajutsu-koekelberg.be/conseil-et-reflexion-du-jour/le-mokuso/](https://www.wajutsu-koekelberg.be/conseil-et-reflexion-du-jour/le-mokuso/ "https://www.wajutsu-koekelberg.be/conseil-et-reflexion-du-jour/le-mokuso/"))

Mokuso est une phase importante de la pratique d’un art martial.

avant le salut rituel au début de la séance et à la fin d’un entraînement à l’injonction « Mokuso

» donné par le **Shihan** ou le **Senseï** . C’est un merveilleux outil qui permet d’ouvrir ou de

conclure la séance de cours.

Le Mokuso du début sert à faire le calme dans son esprit, à laisser le monde et ses problèmes

et préoccupations à l’extérieur du Dojo, et à se concentrer sur la séance à venir.

Le Mokuso de la fin sert à relâcher le corps et l’esprit de l’activité intense de la séance, à faire le vide et retrouver la vie extérieure avec sérénité. Le Mokuso est donc l’instant où le wa- jutsuka s’exerce à la maîtrise de son esprit en éliminant la plupart des perturbations physiques et psychiques liées au contact avec l’environnement.

Mokuso signifie littéralement « **penser en silence** ».

Les origines exactes du Mokuso ne sont pas connues. Il semble cependant que le Mokuso ait été introduit assez tôt dans les arts martiaux.

Les buts du Mokuso sont exprimés par l’expression « _Kokyu wo totonoeru, kokoro wo totonoeru_ »

dans laquelle Kokyu est **la respiration**, Kokoro, **le coeur**, **l’esprit** et le verbe _Totonoeru_ signifie arranger, ajuster mais aussi préparer.

Ajuster sa respiration : ce qui est important c’est de se concentrer sur sa posture (_dos bien droit en alignant bien la colonne vertébrale_), et sur sa respiration.

Respirer régulièrement est en général un exercice recommandé pour se détendre. De ce point de vue, il constitue une excellente introduction à une séance d’activité physique intense. Il ne s’agit pas à proprement parler d’un exercice respiratoire, il n’est donc pas nécessaire de pousser sa respiration.

Le Mokuso se pratique en Seiza, mais aussi en position debout :

* jambes à écartement des épaules
* et 1 pied légèrement avancé (posture confortable)

Au signal du Mokuso, joindre les mains en coupe, paumes vers le haut en déposant la main gauche dans la main droite, pouces joints ne faisant ni mont ni vallées, en fermant les yeux.

* **Inspiration :** inspirez doucement par le nez, pendant 4 secondes, en visualisant l’air descendant jusqu’à votre « seika-tanden » (c’est le point situé à environ 3 cm sous votre nombril, à l’intérieur de votre corps. C’est en quelque sorte votre centre de gravité). Ceci permet une respiration profonde. Vos épaules ne bougent pas, c’est votre abdomen qui se gonfle sous la poussée du diaphragme qui s’abaisse. Ceci vous donne un plus grand volume d’air qu’une respiration thoracique (en gonflant les poumons).

* **Maintien :** à la fin de la phase d’inspiration, retenez l’air, visualisez le dans le « seïka- tanden », pendant 4 secondes. Relâchez bien tous vos muscles, notamment les épaules, le visage, le cou, tout doit converger vers le « seïka-tanden ».

* **Expiration :** Expirez très lentement par la bouche, légèrement entrouverte, pendant 8 secondes, en contrôlant le flux avec votre abdomen. Plus l’expiration sera lente, plus l’impression de calme et de détente sera ressentie. N’expirez pas à fond, vous auriez une sensation d’asphyxie alors que tout doit se passer en douceur. Lorsque vos poumons sont vides sans contraction d’aucune sorte, vous recommencez le cycle d’inspiration. Respirez ainsi jusqu’au signal de fin « Mokuso yame ».

![](/images/2022-06-01_19-06.png)

Dans un premier temps, il est intéressant de se concentrer sur le fait de laisser au vestiaire tout ce qui pourrait perturber la séance. Visualisez-vous serein, en paix, prêt à recevoir et partager afin d’éveiller l’esprit créateur en soi. En fin de séance, ayez de la gratitude envers vous-même, votre senseï, votre senpai, votre famille et visualisez-vous avec ce que vous allez remettre dans ce sac en quittant le dojo. Faites le calme intérieur grâce à la concentration sur la respiration et préparez-vous à retrouver l’extérieur.

_Cet exercice est des plus difficiles et demande concentration et entraînement._

Au bout d’un certain temps, votre esprit fera cela de manière presque automatique, le fil ténu pour se raccrocher restera alors le comptage qui permettra de ne pas diverger vers les pensées multiples qui ne demandent qu’à surgir et perturber notre belle sérénité.

L’idée générale étant en quelque sorte de « **nettoyer** » son esprit pour aborder l’état d’esprit **Keiko** (_le cours, le travail, la pratique, l’étude des choses anciennes)_ que les Japonais appellent « **Mushin** » (_sans sentiment, sans pensée_).