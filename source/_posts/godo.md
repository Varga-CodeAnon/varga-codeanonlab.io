---
title: Godo geiko à la maison
date: 2022/03/10 20:00:00
categories: Kendo
tags:
- Évènements
top: false
---

![](/images/godo.jpeg)


Bonsoir à tous,

C'est à notre tour d'organiser le futur godo geiko en région toulousaine et on s'y prend un peu en avance.
Le week-end prochain, ce sont les compétitions inter-régions et on souhaite à tous les participants de prendre un maximum de plaisir! Elles seront suivies des 2 semaines de vacances de février.
On vous propose donc de venir nous retrouver avec un grand plaisir le jeudi 10 Mars à 20h au dojo Voies des Hommes, situé 8 rue Claude Gonin 31400 Toulouse.

Ce godo geiko sera par évidence suivi d'un petit pot de l'amitié !

À bientôt, Jeudi 10 Mars à 20h!

Fabien pour le Bureau Chowakan.