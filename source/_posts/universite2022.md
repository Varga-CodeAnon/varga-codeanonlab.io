---
title: Université d'été du Kendo
date: 2022/07/13 11:00:00
categories: Kendo
tags:
- Évènements
top: false
---

![](/images/universite2022.jpg)

DATE : 13 au 17 Juillet 2022

LIEU :
Campus Sport Bretagne Formations 
24 Rue des Marettes, 35800 Dinard


C'est parti pour les 16èmes Universités d'été du Kendo ! 
Et comme toujours, c'est en juillet, à Dinard, le sable chaud, les mouettes, les asageiko et tout et tout !

60 places nous sont réservées actuellement, ainsi que 30 places pour mardi soir (veille de stage).

Plus d'informations dans les temps qui viennent, et en attendant, portez-vous bien !

Formulaire Google pour l'inscription aux UEK 2022 : https://docs.google.com/forms/d/e/1FAIpQLSeJ4_fkVMRjyy8nbZPpADRFRg7684ghHifL2TjBgZf6-dKt4w/viewform

Plus d'informations ici : https://www.facebook.com/events/420002036568719/