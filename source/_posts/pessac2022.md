---
title: Coupe de Pessac
date: 2022/06/25 11:00:00
categories: Kendo
tags:
- Évènements
top: false
---

![](/images/pessac2022.jpg)

25 et 26 juin 2022

LIEU : Stade André Nègre, Avenue des Provinces, 33600 PESSAC

Programme :
- Samedi 11H à 18H : compétition individuelle
- Samedi soir : Repas

- Dimanche 08H à 13H : compétition par équipe
- Dimanche 13H : Repas

Plus d'informations sur le site du dojo : http://kendo.pessac.free.fr/?p=2149