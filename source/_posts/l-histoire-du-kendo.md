---
title: 'Le Kendô à l''ancienne : vraiment mieux que la méthode moderne ?'
categories: ''
tags:
- Articles
top: false
slug: "/article-histoire-kendo"

---
![](/images/fb_img_1634250254375.jpg "Kendo moderne (en haut) VS Kendo Bakumatsu (en bas)")

![](/images/fb_img_1633986483176.jpg)

Si vous êtes un peu anglophone et que vous traînez sur la communauté “World Kendo Network”, vous n’avez pas pu échapper à cette image. Aujourd’hui, je vais l’aborder d’un point de vue sportif et biomécanique, tout en me laissant la latitude d’y apporter un grain de philosophie. Facebook ne me permettant pas d’insérer des images au fur et à mesure de l’article, pour le confort de lecture, 𝗷𝗲 𝘃𝗼𝘂𝘀 𝗿𝗲𝗰𝗼𝗺𝗺𝗮𝗻𝗱𝗲 𝗳𝗼𝗿𝘁𝗲𝗺𝗲𝗻𝘁 𝗱’𝗼𝘂𝘃𝗿𝗶𝗿 𝗹’𝗶𝗺𝗮𝗴𝗲 𝗱𝗮𝗻𝘀 𝘂𝗻 𝗲𝘀𝗽𝗮𝗰𝗲 𝗮̀ 𝗰𝗼̂𝘁𝗲́, sur un deuxième écran ou dans une fenêtre séparée, car j’y ferais régulièrement référence.

L’auteur de ce schéma et ces posts proclame à tout va que le kendo “moderne” n’est pas la meilleure façon de faire de l’escrime japonaise, la première rangée en “L” servant à démontrer ce qu’est la façon de faire actuelle (Gendai), et la seconde, en dessous, ce qu’il en était dans “l’ancien temps” (Bakumatsu).

Ces schémas sont extrêmement riches d’enseignements, et ce sur beaucoup de plans. 𝗣𝗿𝗲𝗺𝗶𝗲̀𝗿𝗲𝗺𝗲𝗻𝘁, 𝗰𝗲 𝘀𝗼𝗻𝘁 𝗹𝗲𝘀 𝗽𝗿𝗲𝗺𝗶𝗲𝗿𝘀 𝘀𝗰𝗵𝗲́𝗺𝗮𝘀 𝗾𝘂𝗲 𝗷𝗲 𝘃𝗼𝗶𝘀 𝗾𝘂𝗶 𝗽𝗿𝗲𝗻𝗻𝗲𝗻𝘁 𝗲𝗻 𝗰𝗼𝗺𝗽𝘁𝗲 𝗹𝗮 𝗿𝗲́𝗮𝗹𝗶𝘁𝗲́ 𝗱𝗲 𝘁𝗲𝗿𝗿𝗮𝗶𝗻 𝗱𝘂 𝗸𝗲𝗻𝗱𝗼. En effet, sur la première rangée (c’est de celle-ci uniquement dont je vais parler en première partie) nous pouvons constater un nombre de choses qui se retrouvent après analyse méticuleuse de ce qui se passe “en vrai”, mais qui semblent ne pas être enseignées telles quelles dans les dojos de façon générale, pour nous épargner une “mauvaise forme”. 𝗣𝗼𝘂𝗿𝘁𝗮𝗻𝘁, 𝗰’𝗲𝘀𝘁 𝗯𝗲𝗹 𝗲𝘁 𝗯𝗶𝗲𝗻 𝗮𝗶𝗻𝘀𝗶 𝗾𝘂𝗲 𝘁𝗼𝘂𝘀 𝗹𝗲𝘀 𝗰𝗵𝗮𝗺𝗽𝗶𝗼𝗻𝘀 𝗷𝗮𝗽𝗼𝗻𝗮𝗶𝘀, 𝗹𝗲𝘀 𝟴𝗲𝗺𝗲 𝗱𝗮𝗻, 𝗲𝘁 𝗮𝘂𝘁𝗿𝗲𝘀 𝗸𝗲𝗻𝗱𝗼̂𝗸𝗮 𝘁𝗿𝗲̀𝘀 𝗲𝘅𝗽𝗲́𝗿𝗶𝗺𝗲𝗻𝘁𝗲́𝘀 𝗽𝗿𝗮𝘁𝗶𝗾𝘂𝗲𝗻𝘁.

Tame : 𝗟𝗮 𝗷𝗮𝗺𝗯𝗲 𝘀𝗲 𝗽𝗹𝗶𝗲 𝗮̀ 𝗹’𝗶𝗻𝗶𝘁𝗶𝗮𝘁𝗶𝗼𝗻 𝗱𝘂 𝗺𝗼𝘂𝘃𝗲𝗺𝗲𝗻𝘁. En réalité, c’est tout à fait logique, un genou totalement étendu ne permet pas de l’utiliser pour se propulser depuis le sol, le plier permet non seulement de se donner une plus grande amplitude d’un point de vue articulaire au genou, sur laquelle développer de la puissance, mais aussi à la hanche, car elle se retrouve elle aussi moins étendue. Enfin point non négligeable, le triceps sural (muscle du mollet) peut se charger d’une “pré-tension” nécessaire à un mouvement de propulsion rapide. 𝗣𝗹𝗶𝗲𝗿 𝗹𝗮 𝗷𝗮𝗺𝗯𝗲 𝗻’𝗲𝘀𝘁 𝗱𝗼𝗻𝗰 𝗽𝗮𝘀 𝘂𝗻 𝗱𝗲́𝗳𝗮𝘂𝘁 : 𝗰’𝗲𝘀𝘁 𝘂𝗻𝗲 𝗽𝗿𝗲́𝗽𝗮𝗿𝗮𝘁𝗶𝗼𝗻.

Hikite : Ici, c’est le seul point du schéma où je serais en désaccord, très exactement au niveau de la verticalité du shinai, 𝗰𝗲𝗹𝗮 𝘃𝗮𝗿𝗶𝗲 𝗴𝗿𝗮𝗻𝗱𝗲𝗺𝗲𝗻𝘁 𝗲𝗻𝘁𝗿𝗲 𝗹𝗲𝘀 𝗽𝗿𝗮𝘁𝗶𝗾𝘂𝗮𝗻𝘁𝘀. Il n’y a qu’à voir à quel point un “men” entre Teramoto et Takanabe est différent pour s’en convaincre. Cela vient du fait que les pratiquants ont une grande diversité de mouvements réalisables : 𝗿𝗼𝘁𝗮𝘁𝗶𝗼𝗻 𝗮𝘂 𝗻𝗶𝘃𝗲𝗮𝘂 𝗱𝗲𝘀 𝗽𝗼𝗶𝗴𝗻𝗲𝘁𝘀, 𝗱𝗲𝘀 𝗰𝗼𝘂𝗱𝗲𝘀, 𝗱𝗲𝘀 𝗲́𝗽𝗮𝘂𝗹𝗲𝘀, 𝘃𝗼𝗶𝗿𝗲 𝗹𝗲𝘀 𝘁𝗿𝗼𝗶𝘀 𝗮̀ 𝗹𝗮 𝗳𝗼𝗶𝘀 !

Tobi-men : Il est particulièrement intéressant de noter que la tête part vers l’arrière. Et c’est en réalité le cas, si on projette les épaules vers l’avant (et c’est ce que nous faisons pour atteindre la cible, même en essayant “d’envoyer avec les hanches”, car à haute vitesse, le corps n’a plus le temps de se soucier de ce qu’on voudrait faire : il sélectionne le plus efficace), 𝗰’𝗲𝘀𝘁 𝗰𝗲𝘁𝘁𝗲 𝗽𝗮𝗿𝘁𝗶𝗲 𝗱𝗲 𝗻𝗼𝘁𝗿𝗲 𝗰𝗼𝗿𝗽𝘀 𝗾𝘂𝗶 𝘃𝗮 𝗲𝗻𝘁𝗿𝗮𝗶̂𝗻𝗲𝗿 𝗹𝗲 𝗿𝗲𝘀𝘁𝗲, 𝗲𝘁 𝗲̂𝘁𝗿𝗲 𝗳𝗮𝘁𝗮𝗹𝗲𝗺𝗲𝗻𝘁 𝗱𝗲𝘃𝗮𝗻𝘁. Un peu comme quand on tire un enfant par le bras un peu trop fort. Mêlé au réflexe archaïque d’extension (étendre les muscles du cou et les coudes en même temps donne plus de force à ces dits muscles, c’est un réflexe qui intervient chez le nourrisson pour lui permettre de se relever de la position “couchée”, mais nous avons des preuves de sa conservation même à l’âge adulte 𝘈𝘳𝘦 𝘦𝘧𝘧𝘦𝘤𝘵𝘴 𝘰𝘧 𝘵𝘩𝘦 𝘴𝘺𝘮𝘮𝘦𝘵𝘳𝘪𝘤 𝘢𝘯𝘥 𝘢𝘴𝘺𝘮𝘮𝘦𝘵𝘳𝘪𝘤 𝘵𝘰𝘯𝘪𝘤 𝘯𝘦𝘤𝘬 𝘳𝘦𝘧𝘭𝘦𝘹𝘦𝘴 𝘴𝘵𝘪𝘭𝘭 𝘷𝘪𝘴𝘪𝘣𝘭𝘦 𝘪𝘯 𝘩𝘦𝘢𝘭𝘵𝘩𝘺 𝘢𝘥𝘶𝘭𝘵𝘴?(2013) ), cela semble logique et cohérent.

Fumikomi : Je juge cette partie extrêmement enrichissante. 𝗜𝗹 𝗲𝘀𝘁 𝗱’𝘂𝘀𝗮𝗴𝗲 𝗾𝘂𝗲 𝗽𝗼𝘂𝗿 𝗲𝗻𝘀𝗲𝗶𝗴𝗻𝗲𝗿 𝗹𝗲 𝗸𝗲𝗻𝗱𝗼, 𝗻𝗼𝘂𝘀 𝗶𝗻𝘀𝗶𝘀𝘁𝗶𝗼𝗻𝘀 𝘀𝘂𝗿 𝗹𝗲 “𝗸𝗶-𝗸𝗲𝗻-𝘁𝗮𝗶 𝗻𝗼 𝗶𝗰𝗵𝗶”, 𝗹’𝗲𝘀𝗽𝗿𝗶𝘁, 𝗹𝗲 𝗰𝗼𝗿𝗽𝘀 𝗲𝘁 𝗹𝗲 𝘀𝗮𝗯𝗿𝗲 𝗲𝗻 𝗺𝗲̂𝗺𝗲 𝘁𝗲𝗺𝗽𝘀. Pour la majorité des enseignants que j’ai croisés, l’insistance était grande sur le fait de coordonner la frappe du pied (fumikomi) et l’arrivée sur la cible du shinai.

Et pourtant, regardez du kendô au ralenti si vous voulez vous en convaincre, 𝗰̧𝗮 𝗻’𝗲𝘀𝘁 𝗝𝗔𝗠𝗔𝗜𝗦 𝗹𝗲 𝗰𝗮𝘀. Même chez les plus gradés des plus gradés, 𝗹𝗮 𝗳𝗿𝗮𝗽𝗽𝗲 𝗱𝘂 𝗽𝗶𝗲𝗱 𝗮𝗿𝗿𝗶𝘃𝗲 𝘀𝘆𝘀𝘁𝗲́𝗺𝗮𝘁𝗶𝗾𝘂𝗲𝗺𝗲𝗻𝘁 𝗔𝗣𝗥𝗘𝗦 𝗹𝗲 𝘀𝗵𝗶𝗻𝗮𝗶̈.

Je me souviens avoir toujours été perplexe à ce sujet, et poser la question à nombre de professeurs qui m’ont toujours soutenu l’unité des deux frappes coordonnées. C’est feu Jean-Pierre Raick, mes respects à ce grand monsieur, qui m’en a donné l’explication la plus simple et qui m’a ,de fait, complètement rassuré en une seule phrase prononcée lors de l’un de ses stages : “𝗦𝗶 𝗹𝗲 𝘀𝗮𝗯𝗿𝗲 𝗮𝗿𝗿𝗶𝘃𝗲 𝗮𝘃𝗮𝗻𝘁 𝗹𝗲 𝗽𝗶𝗲𝗱, 𝗰’𝗲𝘀𝘁 𝘁𝗼𝘂𝘁 𝗮̀ 𝗳𝗮𝗶𝘁 𝗻𝗼𝗿𝗺𝗮𝗹 𝗲𝘁 𝗻𝗮𝘁𝘂𝗿𝗲𝗹, 𝗲𝗻 𝗿𝗲𝘃𝗮𝗻𝗰𝗵𝗲, 𝘀𝗶 𝗹𝗲 𝗽𝗶𝗲𝗱 𝗮𝗿𝗿𝗶𝘃𝗲 𝗮𝘃𝗮𝗻𝘁 𝗹𝗲 𝘀𝗮𝗯𝗿𝗲, 𝗰’𝗲𝘀𝘁 𝗾𝘂’𝗶𝗹 𝘆 𝗮 𝘂𝗻 𝘃𝗿𝗮𝗶 𝘀𝗼𝘂𝗰𝗶”.

Non, il n’a pas eu besoin de développer plus que ça, j’avais déjà un premier élément de taille : je n’étais pas fou.

La raison biologique et mécanique, je l’ai trouvée par moi-même, plus tard, et elle réside en deux aspects, découlant l’un de l’autre.

\- Le premier, c’est que quand nous envoyons une information depuis notre cerveau à tous nos muscles, ce qui réagit en premier, 𝗰’𝗲𝘀𝘁 𝗰𝗲 𝗾𝘂𝗶 𝗲𝘀𝘁 𝗽𝗹𝘂𝘀 𝗽𝗿𝗼𝗰𝗵𝗲 𝗱𝗲 𝗰𝗲 𝗱𝗲𝗿𝗻𝗶𝗲𝗿. À une énorme pression, telle qu’on peut en avoir face à un adversaire que l’on ne sous-estime pas, on n’a pas le temps de réfléchir à envoyer tout son corps en même temps : résultat, 𝗹𝗲𝘀 𝗺𝗮𝗶𝗻𝘀 𝗽𝗮𝗿𝘁𝗲𝗻𝘁 𝗮𝘃𝗮𝗻𝘁 𝗹𝗲𝘀 𝗽𝗶𝗲𝗱𝘀 𝗮̀ 𝗹’𝗼𝗯𝗷𝗲𝗰𝘁𝗶𝗳… 𝗘𝘁 𝗱𝗼𝗻𝗰 𝗮𝗿𝗿𝗶𝘃𝗲𝗻𝘁 𝗮𝘂𝘀𝘀𝗶 𝗮𝘃𝗮𝗻𝘁.

\- Le second, c’est que le fumikomi n’a pas qu’un rôle de “signal” du ki-ken-tai,𝗶𝗹 𝗽𝗲𝗿𝗺𝗲𝘁 𝗮𝘂𝘀𝘀𝗶 𝗱𝗲 𝘀𝗲 𝘀𝘁𝗮𝗯𝗶𝗹𝗶𝘀𝗲𝗿 𝘃𝗶𝘀-𝗮̀-𝘃𝗶𝘀 𝗱𝗲 𝗻𝗼𝘁𝗿𝗲 𝗰𝗵𝘂𝘁𝗲 𝗲𝗻 𝗮𝘃𝗮𝗻𝘁 𝗽𝗿𝗼𝘃𝗼𝗾𝘂𝗲́𝗲 𝗽𝗮𝗿 𝗹𝗲 𝗱𝗲́𝗽𝗹𝗮𝗰𝗲𝗺𝗲𝗻𝘁 𝗱𝗲 𝗻𝗼𝘁𝗿𝗲 𝗰𝗲𝗻𝘁𝗿𝗲 𝗱𝗲 𝗴𝗿𝗮𝘃𝗶𝘁𝗲́ 𝗮̀ 𝗰𝗮𝘂𝘀𝗲 𝗱𝗲𝘀 𝗯𝗿𝗮𝘀 𝗾𝘂𝗶 𝗽𝗮𝗿𝘁𝗲𝗻𝘁 𝗲𝗻 𝗮𝘃𝗮𝗻𝘁.

Le rétablissement est donc aussi naturellement assuré 𝗽𝗮𝗿 𝗹𝗲 𝗿𝗲𝗯𝗼𝗻𝗱 𝗱𝘂 𝘀𝗮𝗯𝗿𝗲 𝗾𝘂𝗶 𝗿𝗲𝘃𝗶𝗲𝗻𝘁 𝗹𝗲́𝗴𝗲̀𝗿𝗲𝗺𝗲𝗻𝘁 𝘃𝗲𝗿𝘀 𝗹’𝗮𝗿𝗿𝗶𝗲̀𝗿𝗲. Cela nous permet de pouvoir plus rapidement enchaîner sur une autre technique, de rester disponible, si la première n’a pas porté ses fruits.

Maintenant que nous avons posé les bases du “kendô moderne”... Parlons de l’ancien temps et de sa présumée supériorité.

Car oui, j’y ai précédemment fait allusion, mais je le répète : L’ancien kendo serait supérieur.

Très bien, comparons donc les deux formes, et regardons si de tous points de vue, l’ancienne façon de faire est au-dessus, et si les propos de l’auteur à l’origine de ces posts sont en accord avec cela.

Tohma : La première différence, et pas des moindres, réside dans la distance abordée. Je n’en ai volontairement pas parlé en première partie, mais je pense néanmoins qu’il a vu encore une fois très juste : 𝗶𝗹 𝗲𝘀𝘁 𝗶𝗺𝗽𝗼𝘀𝘀𝗶𝗯𝗹𝗲 𝗱𝗲 𝗽𝗮𝗿𝘁𝗶𝗿 𝗱’𝘂𝗻𝗲 𝗴𝗿𝗮𝗻𝗱𝗲 𝗱𝗶𝘀𝘁𝗮𝗻𝗰𝗲 𝗰𝗼𝗺𝗺𝗲 𝗰𝗲𝗿𝘁𝗮𝗶𝗻𝘀 𝗽𝗿𝗼𝗳𝗲𝘀𝘀𝗲𝘂𝗿𝘀 𝗻𝗼𝘂𝘀 𝗹𝗲 𝗱𝗲𝗺𝗮𝗻𝗱𝗲𝗻𝘁, 𝘀𝗮𝗻𝘀 𝗮𝘃𝗼𝗶𝗿 𝗳𝗮𝗶𝘁 𝘂𝗻 𝘀𝗲𝗺𝗲 𝗮𝘂 𝗽𝗿𝗲́𝗮𝗹𝗮𝗯𝗹𝗲 𝗮𝘃𝗲𝗰 𝗹𝗮 𝘁𝗲𝗰𝗵𝗻𝗶𝗾𝘂𝗲 “𝗺𝗼𝗱𝗲𝗿𝗻𝗲”. Les seules fois ou cela fonctionne… C’est quand les deux adversaires lancent en même temps et réduisent de fait la distance “par les deux bouts”. La technique de l’ancien temps semble changer la donne, et rien que pour cela, c’est très intéressant. Le point ne va néanmoins pas à l’ancien kendô pour autant : 𝗰’𝗲𝘀𝘁 𝗷𝘂𝘀𝘁𝗲 𝘂𝗻𝗲 𝘁𝗲𝗰𝗵𝗻𝗶𝗾𝘂𝗲 𝗱𝗶𝗳𝗳𝗲́𝗿𝗲𝗻𝘁𝗲, 𝗽𝗮𝘀 𝘀𝘂𝗽𝗲́𝗿𝗶𝗲𝘂𝗿𝗲.

Yorimi - Nuki : On peut le voir à partir d’ici, la technique du kendo “Bakumatsu” repose sur une autre mécanique que celle du kendô “Gendai”. Au lieu d’utiliser la puissance musculaire de la jambe arrière, 𝗼𝗻 𝗽𝗿𝗲́𝗳𝗲̀𝗿𝗲 𝘀𝗲 𝗱𝗲́𝘀𝗲́𝗾𝘂𝗶𝗹𝗶𝗯𝗿𝗲𝗿 𝘃𝗲𝗿𝘀 𝗹’𝗮𝘃𝗮𝗻𝘁. L’avantage étant de pouvoir conserver plus longtemps durant sa phase d’accélération la pointe devant en guise de protection, 𝗰𝗮𝗿 𝘀𝗮 𝗺𝗮𝘀𝘀𝗲 𝗲𝘀𝘁 𝘂𝗻 𝗼𝘂𝘁𝗶𝗹 𝘀𝘂𝗽𝗽𝗹𝗲́𝗺𝗲𝗻𝘁𝗮𝗶𝗿𝗲 𝗽𝗼𝘂𝗿 𝗽𝗿𝗲𝗻𝗱𝗿𝗲 𝗱𝗲 𝗹𝗮 𝘃𝗶𝘁𝗲𝘀𝘀𝗲.

Pourquoi pas. Mais l’auteur dit aussi qu’avec un armé plus court, l’adversaire a beaucoup moins le temps de réagir, ce qui rendrait la technique supérieure. C’est il me semble, une allégation non supportée. La phase de prise de vitesse est beaucoup plus importante dans le kendô bakumatsu, 𝗲𝘁 𝗰𝗼𝗺𝗺𝗲 𝗲𝗹𝗹𝗲 𝗿𝗲𝗽𝗼𝘀𝗲 𝘀𝘂𝗿 𝘂𝗻 𝗱𝗲́𝘀𝗲́𝗾𝘂𝗶𝗹𝗶𝗯𝗿𝗲 𝘃𝗲𝗿𝘀 𝗹’𝗮𝘃𝗮𝗻𝘁, 𝗲𝗹𝗹𝗲 𝗲𝘀𝘁 𝗯𝗶𝗲𝗻 𝗺𝗼𝗶𝗻𝘀 “𝗰𝗼𝗻𝘁𝗿𝗼̂𝗹𝗮𝗯𝗹𝗲”. Ainsi il n’est pas rare de voir des kenshi “Gendai” durant divers championnats enchaîner les phase de “tame”, il serait impossible de faire cela à un telle vitesse pour une phase “yorimi - nuki”. De fait, on peut facilement “voir venir” un pratiquant bakumatsu,𝗾𝘂𝗶 𝘀𝗲 𝗹𝗮𝗻𝗰𝗲 𝗽𝗹𝘂𝘁𝗼̂𝘁 𝗰𝗼𝗺𝗺𝗲 𝘂𝗻 𝗿𝗵𝗶𝗻𝗼𝗰𝗲́𝗿𝗼𝘀 𝗾𝘂’𝘂𝗻𝗲 𝗽𝗮𝗻𝘁𝗵𝗲̀𝗿𝗲. Si la technique en particulier est (peut-être) moins prévisible, 𝗹𝗲 𝗳𝗮𝗶𝘁 𝗾𝘂’𝗶𝗹 𝘆 𝗲𝗻 𝗮𝗶𝘁 𝘂𝗻𝗲 𝗹’𝗲𝘀𝘁 𝗯𝗲𝗮𝘂𝗰𝗼𝘂𝗽 𝗽𝗹𝘂𝘀. Ainsi, il ne s’agirait probablement plus d’envisager la contre-attaque appropriée, 𝗺𝗮𝗶𝘀 𝘀𝗶𝗺𝗽𝗹𝗲𝗺𝗲𝗻𝘁 𝗹’𝗲𝘀𝗾𝘂𝗶𝘃𝗲 𝗲𝘁 𝗹𝗲 𝗰𝗼𝗻𝘁𝗿𝗼̂𝗹𝗲 𝗽𝗼𝘂𝗿 𝗱𝗲́𝗰𝗹𝗲𝗻𝗰𝗵𝗲𝗿 𝘂𝗻𝗲 𝗼𝗽𝗽𝗼𝗿𝘁𝘂𝗻𝗶𝘁𝗲́ 𝗼𝗷𝗶-𝘄𝗮𝘇𝗮.

Autre point important relié à celui-ci, la phase d’Hikite en kendô Gendai peut servir aux pratiquants à prolonger la menace. Je pense aux techniques katsugi par exemple, grâce auxquelles Teramoto a d’ailleurs gagné les championnats du japon face à Takanabe. 𝗥𝗲́𝘀𝘂𝗺𝗲𝗿 𝗰𝗲𝘁 𝗮𝗿𝗺𝗲́ 𝗮̀ 𝘂𝗻𝗲 𝗳𝗮𝗶𝗯𝗹𝗲𝘀𝘀𝗲, 𝗹𝗮̀ 𝗼𝘂̀ 𝗶𝗹 𝗽𝗲𝘂𝘁 𝗱𝗲𝘃𝗲𝗻𝗶𝗿 𝘂𝗻 𝗿𝗲𝗱𝗼𝘂𝘁𝗮𝗯𝗹𝗲 𝗼𝘂𝘁𝗶,𝗹 𝗺𝗲 𝘀𝗲𝗺𝗯𝗹𝗲 𝗲̂𝘁𝗿𝗲 𝘂𝗻𝗲 𝗲𝗿𝗿𝗲𝘂𝗿.

Fumikomi : Celui-ci arrive AVANT la coupe, afin de profiter du pied frappant pour encore prendre de la vitesse derrière et rogner le dernier espace séparant le pratiquant de la cible. En tant que curieux de nature, d’enthousiaste à propos du kendô, cette créativité me fascine et me stimule. Ainsi, en compétition, en passage de grade, d’aucuns jugeraient que cette technique est imparfaite, mal coordonnée. Pourtant, elle faisait sens du temps du bakumatsu, 𝗲𝘁 𝘀𝗶 𝗹’𝗼𝗻 𝘀𝗲 𝗳𝗶𝗲 𝘂𝗻𝗶𝗾𝘂𝗲𝗺𝗲𝗻𝘁 𝗮̀ 𝗹𝗮 𝗻𝗼𝘁𝗶𝗼𝗻 𝗱𝗲 𝘁𝗿𝗮𝗱𝗶𝘁𝗶𝗼𝗻 𝗮𝗻𝗰𝗲𝘀𝘁𝗿𝗮𝗹𝗲 𝗱𝘂 𝗸𝗲𝗻𝗱𝗼, 𝗲𝗹𝗹𝗲 𝗲𝘀𝘁 𝗽𝗹𝘂𝘀 𝗰𝗼𝗿𝗿𝗲𝗰𝘁𝗲 ! C’est principalement d’ailleurs cette phase qui permet à la distance de départ (tohma) d’être plus importante. En effet, on gagne facilement un demi pas en utilisant l’autre pied. Je rajouterais que le fait que la phase d’armé soit moins ample n’est sans doute pas dérangeant : en se déséquilibrant vers l’avant, une partie du poids du corps est transférée à la lame, qui s’abat donc avec largement assez de force sur la cible visée.

Zanshin : Si l’objectif de l’auteur était de montrer que la phase de zanshin est supérieure avec le kendo bakumatsu car l’on garde notre sabre devant, je pense que c’est plutôt un mauvais exemple. Le corps est encore déséquilibré vers l’avant, mais pire encore : 𝗹𝗲𝘀 𝗱𝗲𝘂𝘅 𝗽𝗶𝗲𝗱𝘀 𝘀𝗼𝗻𝘁 𝗲𝘅𝗮𝗰𝘁𝗲𝗺𝗲𝗻𝘁 𝗮𝘂 𝗺𝗲̂𝗺𝗲 𝗲𝗻𝗱𝗿𝗼𝗶𝘁, 𝗿𝗲́𝗱𝘂𝗶𝘀𝗮𝗻𝘁 𝗱𝗿𝗮𝘀𝘁𝗶𝗾𝘂𝗲𝗺𝗲𝗻𝘁 𝗹𝗲 𝗽𝗼𝗹𝘆𝗴𝗼𝗻𝗲 𝗱𝗲 𝘀𝘂𝘀𝘁𝗲𝗻𝘁𝗮𝘁𝗶𝗼𝗻. Une simple pichenette latérale suffirait à déséquilibrer ce combattant, s’il a le malheur d’avoir raté sa coupe. Sans compter bien sûr la plus grande difficulté à pouvoir enchaîner ou corriger la technique.

Rappelons enfin le but du kendô, que nous avons trop souvent tendance à oublier :

“𝗙𝗮𝗶𝗿𝗲 𝗱𝗲 𝗻𝗼𝘂𝘀 𝗱𝗲 𝗺𝗲𝗶𝗹𝗹𝗲𝘂𝗿𝘀 𝗲̂𝘁𝗿𝗲𝘀 𝗵𝘂𝗺𝗮𝗶𝗻𝘀 𝗴𝗿𝗮̂𝗰𝗲 𝗮𝘂𝘅 𝗽𝗿𝗶𝗻𝗰𝗶𝗽𝗲𝘀 𝗱𝗲 𝗹’𝗲𝘀𝗰𝗿𝗶𝗺𝗲 𝗷𝗮𝗽𝗼𝗻𝗮𝗶𝘀𝗲”

(version longue sur le site de la fédération japonaise “purpose of kendô”)

L’objectif n’est absolument pas “devenir d’excellents escrimeurs dans le monde réel capables d’être efficaces un sabre à la main”. En revanche, comme dit dans le tout premier article de cette page Facebook, ( ici : [https://www.facebook.com/LeKendoFrancais/posts/104873328543495](https://www.facebook.com/LeKendoFrancais/posts/104873328543495 "https://www.facebook.com/LeKendoFrancais/posts/104873328543495") ) cela ne doit pas nous empêcher de rechercher l’excellence technique en kendô, c’est même plutôt ce qui nous est demandé.

Résumons donc ensemble les avantages et les inconvénients du Bakumatsu kendo face au Gendai avec cet éclairage :

\- Distance d’attaque différente (neutre)

\- Moins de contrôle une fois l’action partie (neutre, car cela pourrait aussi nous inciter à plus de sutemi).

\- Moins de lisibilité des gestes d’armée (neutre, car cela supprime une partie des opportunités de menace)

\- La tête qui reste en avant (point positif pour éviter les blessures avec un contrôle sur tsuki, même si d’un point de vue biomécanique, ça n’est pas plus dangereux).

\- Phase de zanshin en déséquilibre (point négatif évident pour la fragilité de cette position)

De façon générale donc, nous nous retrouvons avec une approche qui ne me semble ni négative, ni positive. 𝗘𝗻 𝗿𝗲𝘃𝗮𝗻𝗰𝗵𝗲, 𝗲𝘁 𝗰𝗲 𝘀𝗲𝗿𝗮 𝗺𝗮 𝗰𝗼𝗻𝗰𝗹𝘂𝘀𝗶𝗼𝗻, 𝗶𝗹 𝘀𝗲𝗿𝗮𝗶𝘁 𝗲𝘅𝘁𝗿𝗲̂𝗺𝗲𝗺𝗲𝗻𝘁 𝗽𝗼𝘀𝗶𝘁𝗶𝗳 𝗾𝘂𝗲 𝗻𝗼𝘂𝘀 𝗹𝗮 𝗿𝗲́𝗶𝗻𝘁𝗿𝗼𝗱𝘂𝗶𝘀𝗶𝗼𝗻𝘀 𝗱𝗮𝗻𝘀 𝗻𝗼𝘁𝗿𝗲 𝗲𝗻𝘀𝗲𝗶𝗴𝗻𝗲𝗺𝗲𝗻𝘁, 𝗲𝘁 𝗻𝗼𝘁𝗿𝗲 𝗳𝗮𝗰̧𝗼𝗻 𝗱𝗲 𝗷𝘂𝗴𝗲𝗿 𝗹𝗲 𝗸𝗲𝗻𝗱𝗼̂.

Pourquoi ? Car cela nous offrirait une technique avec :

\- Une distance d’attaque différente.

\- Un seme différent.

\- Des notions de prise de risques (sutemi) accrues !

Je pense que nous n’aurions qu’à gagner à augmenter notre répertoire technique, et si les ashi barai et autres luttes au corps à corps sont tolérées dans les championnats entre polices, pourquoi pas les men bakumatsu ?

Cessons de chercher à savoir “qui a la plus grosse”, 𝗲𝘁 𝗶𝗻𝘁𝗲́𝗴𝗿𝗼𝗻𝘀 𝗶𝗻𝘁𝗲𝗹𝗹𝗶𝗴𝗲𝗺𝗺𝗲𝗻𝘁 𝘁𝗼𝘂𝘁 𝗰𝗲 𝗾𝘂𝗶 𝗽𝗲𝘂𝘁 𝗻𝗼𝘂𝘀 𝗱𝗼𝗻𝗻𝗲𝗿 𝘂𝗻 𝗮𝘃𝗮𝗻𝘁𝗮𝗴𝗲.

Et vous, vous en pensez quoi ?

_Article rédigé par Ghais sous le pseudo "Le kendo français" sur_ [_facebook_](https://www.facebook.com/104851408545687/posts/129071096123718/)