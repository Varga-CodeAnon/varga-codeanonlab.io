---
title: Stage au vignoble Nantais avec sensei Jonathan Bertout
date: 2022/04/30 11:00:00
categories: Kendo
tags:
- Évènements
top: false
---

![](/images/vignoble2022.jpg)

L'ESVN vous invite à participer à sa 1ère édition du STAGE DU VIGNOBLE !

Le stage sera encadré par Jonathan BERTOUT, 5e DAN et ancien capitaine de l'équipe de France de Kendo, et se tiendra les 30 avril et 1er mai 2022.

Une dégustation de Muscadet (fleuron du vignoble nantais) vous sera également proposée pendant le week-end, ainsi que des possibilités d'hébergement.

LIEU : Lycée Charles Péguy, 3 Rue de la Sèvre, 44190 Gorges

Stage réservé aux armurés.

LIEN pour s’inscrire :
https://docs.google.com/forms/d/e/1FAIpQLSc8muofRGHsSc8iEUj9z-nXuIy_fdoF1RRox1Tal3DfizCUZw/viewform

Plus d'informations sur le site du dojo : https://www.esvn.fr/