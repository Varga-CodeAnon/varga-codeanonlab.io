---
categories: Kendo
title: Godo geiko à SKT
date: 2022/05/25 20:00:00
tags: 
- Évènements
top: false

---
Bonjour,

  
Comme tous les mois, il y a godo geiko. Ce mois-ci, c'est **Seigakukan** qui l'organise, avec sa tenue le 25 Mai (Mercredi prochain). Tous les détails sont ci-dessous.

Si vous pouvez y aller, ne vous privez pas!! Ca serait sympa que l'on soit 5 ou 6!

  
C'est chowakan qui accueillera le dernier godo geiko de la saison, celui du mois de Juin.

  
Bonne journée

  
Fabien, pour le Bureau Chowakan.

![](/images/proxy-image.jpeg)
