---
title: Stage féminin à Toulouse avec sensei Sabine Péré
date: 2022/04/03 11:00:00
categories: Kendo
tags:
- Évènements
top: false
---

![](/images/feminin2022.jpg)

Un Stage Féminin de Kendo se déroulera le Dimanche 3 avril 2022 au :
> 1 Rue Abel Boyer,
> 31770 Colomiers

Il sera encadré par **Sabine** **Péré** (6ème Dan)

### Horaires

- Ouverture des portes : 9h30
- Matin : 10h-12h
- Après-midi :13h30-15h30
- Ji-geiko mixte ouvert à tous : 15h30 à 16h30
- Pot commun de l’amitié pour clôturer le stage jusqu’à 18h

### Informations et inscriptions

Stage ouvert à tous les licenciés FFJDA de l’année en cours et dans le respect des mesures sanitaires en vigueur.

Vous pouvez nous faire parvenir vos questions et suggestions de points à aborder via le formulaire d’inscription ci-dessous.

Inscription obligatoire en ligne via le formulaire ici. pour participer au stage.

Concernant le ji-geiko mixte, si vous souhaitez y participer il est inutile de vous inscrire en ligne. Vous êtes libre de votre venue.

### Contacts : 

Sabine Péré – sabinepere@orange.fr
Laura Nikitine – laura.nikitine@gmail.com

Plus d'informations ici : http://crk-occitanie.org/