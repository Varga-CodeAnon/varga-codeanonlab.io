---
title: De l'art représentant le Kendo
categories: ''
tags:
- Articles
top: false

---
_Article publié par Georges Mc Call en Octobre 2017 sur son site_ [_kenshi247.net_](https://kenshi247.net/blog/2017/10/01/kendo-art/)

_Traduction française : **Fabien Rivaille** Avril 2022_

![](/images/2022-06-01_19-26.png)

Qui n’a pas vu au moins une fois cette gravure lorsqu’on s’intéresse un peu à l’histoire du kendo ? Voici un article qui le présente parfaitement !

![](/images/2022-06-01_19-26_1.png)

Lorsque le Tokugawa-Bakufu fut démantelé en 1867/68, l'enseignement du budo a été bouleversé : les écoles de domaine ont disparu ainsi que l'éphémère Kobusho, et avec cela les instructeurs de budo ont soudainement perdu leur profession. De nombreux (maintenant ex-) samouraïs se sont soudainement retrouvés sans emploi et confrontés à la misère. L'ex-samouraï, instructeur du Kobusho et de Jikishinkage-ryu, le kenshi Sakakibara Kenkichi, fut une personne qui est intervenue pour aider ces guerriers. Il a institué ce qu'on a appelé « Gekken - kogyo », les spectacles de budo publics très populaires. Le mot « Gekken » fait référence à la forme naissante de ce que nous appelons aujourd’hui le kendo. Bien que ce fut principalement des spectacles au sabre, des combats avec d'autres armes ont également eu lieu et des femmes tout comme des étrangers y ont également participé.

Le 26 avril 1873, un événement de 10 jours se produisit dans le quartier d’Asakusa à Tokyo. Cela eut une influence directe sur le développement du kendo moderne sans lequel ce kendo naissant (alors appelé gekken, gekkiken, shinai-uchikomi ou kenjutsu) aurait probablement pu simplement disparaitre : le tout premier Gekkikenkai 撃剣会 ou Gekken Kogyo 撃剣興行. Il s'agissait d'un rassemblement public d'experts du budo se battant devant un public et payés pour le faire. De nombreux combattants de ce premier gekkikenkai étaient d'anciens samouraïs sans travail, mais il comprenait également des femmes et, curieusement, un couple de kenshi non japonais.

Trois gravures sur bois, nommées [Ukiyo-e, ](https://en.wikipedia.org/wiki/Ukiyo-e)furent commandées pour commémorer l'événement, l'artiste étant Utagawa Kuniteru deuxième du nom. Après de nombreuses années de recherche infructueuse d'une copie imprimée de l'un des tirages, j'ai récemment reçu en cadeau un tirage original de 1873 de mon préféré !

![](/images/2022-06-01_19-28.png)

Permettez-moi de présenter brièvement les personnages de mon tirage, en commençant par les deux kenshi avant de passer aux juges, puis en terminant par une courte discussion et une vidéo sur la façon dont les Ukiyo-e ont été/sont fabriqués.

### OGAWA Kiyotake (小川清武)

Le kenshi sur le côté gauche de l'image est Ogawa Kiyotake. Ayant appris un style s’escrime différent au départ, il s'est tourné vers l'étude du jikishinkage-ryu et a été inscrit sur le [banzuke ](http://en.wikipedia.org/wiki/Banzuke)(le lien montre un sumo banzuke et non un gekkikenkai) en tant que kenshi de première classe (一等剣士). Il

a travaillé comme instructeur adjoint au Kobusho (école militaire éphémère dans les années 1850- 1860) puis comme inspecteur de police dans la toute jeune police japonaise entre 1874-1883.

![](/images/2022-06-01_19-29.png)

### AKAMATSU Guntayu (赤松軍太夫)

Akamatsu Guntayu se tient au milieu de l'image, en garde jodan. Son do porte le premier caractère de son nom (赤). Bien qu'il ne soit pas un élève de Sakakibara, il a été répertorié comme un kenshi de première classe comme Ogawa ci-dessus. Akamatsu était originaire du [domaine de Choshu ](http://en.wikipedia.org/wiki/Ch%C5%8Dsh%C5%AB_Domain), un endroit réputé pour son fort kenshi. Il était connu pour avoir assisté à diverses rencontres de gekkikenkai à travers le pays.

![](/images/2022-06-01_19-29_1.png)

### NOMI Teijiro (野見錠次郎) , 1827-1909

Le shinpan (alors appelé miwakeyaku ou gyoji ) qui se tient entre les deux kenshi est Nomi Teijiro. Nomi était le meilleur élève de Sakakibara, et on dit que le Gekken Kogyo était son idée : inquiet par le fait que le budo décline et finisse par disparaitre, tout comme le fait que de nombreux samouraïs

sans travail n'avaient pplus les moyens de manger, il a discuté de cette idée avec Sakakibara. Nomi héritera plus tard à la fois du Jikishinkage-ryu et de la direction du Gekken Kogyo.

![](/images/2022-06-01_19-30.png)

### SAKAKIBARA Kenkichi (榊原健吉), 1830-1894

Sakakibara Kenkichi est assis à l'extrême droite de l'image tenant un éventail. Personne ayant étudié l'histoire du kendo ignore son nom. Il est l'homme qui a promu et dirigé le premier Gekken Kogyo, le directeur du Jikishinkage ryu, ancien instructeur de kenjutsu au gouvernement Kobusho et, plus tard, également instructeur de kenjutsu au sein du Keishicho, entre autres.

![](/images/2022-06-01_19-31.png)

### **Comment sont fabriqués les ukiyo-e ?**

Les Ukiyo-e étaient réalisés et imprimés à grande échelle pour un large public. Il y avait trois personnes impliquées principalement dans la création d'une gravure sur bois. D'abord, l'E-shi, ou

l’artiste, la personne qui fait le dessin. C'est le E-shi dont le nom figure sur l'impression elle-même. La deuxième personne du processus, sans doute la plus importante, est le Hori-shi, le sculpteur. Il prend le dessin original puis le grave sur un bloc de bois utilisé ensuite pour l'impression. La troisième personne est le Suri-shi, l'imprimeur. Il prend le papier à imprimer, mélange les couleurs et place le papier sur le bloc de bois pour produire l'impression finale. Le Suri-shi produisait de nombreuses copies d'ukiyo-e individuels. Bien que produites en grande quantité, chaque impression individuelle était réalisée à la main, ce qui pouvait donc générer des variations dans les couleurs et la texture.

Notez que le style particulier de l’ukiyo-e que j'ai présenté aujourd'hui s'appelle un Nishiki-e, un style plus récent d'estampe sur bois qui utilise un grand nombre de couleurs.

Si tout cela semble compliqué, c'est parce que ça l'est !