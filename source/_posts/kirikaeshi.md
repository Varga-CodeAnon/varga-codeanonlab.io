---
title: Kirikaeshi
categories: ''
tags:
- Articles
top: false

---
_Réflexion de Nagao Susumu (Professeur, Université Meiji) publié dans la partie Kendo's Not-so Common-sense de Kendo Nippon en Janvier 2011. Cet article fut ensuite traduit par Kendo World puis présenté par_ _Budo World en Février 2017._

_Traduction française : **Fabien Rivaille** Avril 2022_

Depuis quand le kirikaeshi fait-il partie de la pratique du kendo ? Pourquoi y a-t-il cet ordre « quatre frappes diagonales vers l'avant et cinq vers l'arrière » ?

Le Kirikaeshi et le kakari-geiko trouvent leurs origines dans une méthode de pratique de l’école ancienne Hokushin Ittō-ryū appelée « uchikomi ». Dans le livre **_Chiba Shūsaku Sensei Jikiden Kenjutsu Meijin-hō_** (1884), on trouve le passage suivant :

_« L'Uchikomi n'est pas quelque chose de populaire dans les autres écoles. Si vous souhaitez vraiment améliorer vos techniques de kenjutsu, vous ne pouvez pas le faire sans pratiquer l'uchikomi. Par conséquent, les débutants de notre école n'étaient pas autorisés à participer immédiatement à des combats réels. Tout ce qu'ils ont fait, c'est de l’uchikomi pendant plus d'un an. En ce qui concerne cette méthode d'entraînement, vous pouvez grandement vous améliorer en frappant puissamment le men de votre adversaire par des séquences rapides alternant frappe à gauche, frappe à droite, ou en frappant le centre du men de votre adversaire, ou en frappant le do de votre adversaire de façon alternée, à gauche puis à droite._

Il continue :

_« Le motodachi ne se contente pas d’attendre et recevoir les frappes du kirikaeshi. Il doit chercher une occasion de riposter contre les men ou kote de l'adversaire, dans le but que les deux partenaires d'entraînement pratiquent avec détermination. »_

À partir de là, on peut supposer que l'uchikomi était une méthode d'entraînement intégrée et axée sur la discipline, semblable à une combinaison de kirikaeshi et de kakari-geiko . Si tel était le cas, cela devait être extrêmement grave et atroce.

La forme de la pratique du kirikaeshi la plus courante aujourd'hui consiste à frapper d'abord le men au centre, puis alterner les frappes sur le côté droit puis gauche (en commençant par le côté gauche du men de l'adversaire ) quatre fois en avançant, puis cinq fois de la même manière sur les côtés gauche et droit en reculant. Après la dernière frappe, l'attaquant continue de reculer pour retrouver sa distance (maai), puis se précipite à nouveau pour frapper le men de l’adversaire au centre, continue avec la même routine et frappe enfin le men de l’adversaire au centre pour terminer. Cependant, comme cela est expliqué dans _Kendō Shidō Yōryō_ et _Kendō Kōshūkai Shiryō_, cette méthode n'est qu'une ligne directrice pour les débutants. Il est également mentionné que le praticien doit être créatif lorsqu'il fait du kirikaeshi, dépendant du niveau de pratique, en augmentant le nombre de frappes, en frappant le côté gauche puis droit du men jusqu'à s'essouffler, s'écraser l'un contre l'autre, etc.

Dans son livre intitulé _Kendo_ (1915), Takano Sasaburō écrit : _« Vous devriez toujours pratiquer le kirikaeshi »_, et il propose l'explication suivante pour décrire les intérêts que l'on peut en tirer :

_« Kirikaeshi est une méthode de pratique essentielle pour apprendre le kendo. Vous deviendrez agile en vous déplaçant vers l'avant, l'arrière, la gauche et la droite, votre corps et vos membres deviendront plus forts, vos mouvements seront décomplexés et votre capacité respiratoire s'améliorera. Vos attaques deviendront plus précises et spontanées, votre intention et votre force physique deviendront unifiées, la force superflue sera supprimée et les personnes ayant moins de force se renforceront. La puissance dans le côté gauche et droit de votre corps s'équilibrera, vos frappes côté ura et omote deviendront uniformes, vous pourrez exécuter des techniques à la vitesse de l'éclair et vous améliorerez votre endurance et votre courage. »_

Aussi, pour résumer le **comment faire kirikaeshi**, Nagao Susumu écrit :

_« Frappez rapidement le men en avant et en arrière alternativement, en vous exerçant de façon vigoureuse à chaque frappe, sans vous arrêter, jusqu'à ce que vous soyez à bout de souffle. Les frappes doivent être grandes et rapides, vos bras et vos jambes doivent être coordonnés et votre esprit et votre mental synchronisés tout en frappant puissamment. Lorsque vos bras s'épuisent ou que vous êtes essoufflé, levez les bras au-dessus de votre tête puis étirez-les vers l'avant, avancez vos pieds et frappez encore plusieurs fois en criant « Men! » Alors seulement après vous serez autorisés à vous reposer. »_

Cela signifie qu'à l'origine, le kirikaeshi n'était pas un exercice dont le nombre de frappes était limité. Ce qui importait plus que tout, c'était de frapper vigoureusement avec l'esprit et le mental synchronisés jusqu'à ce que les bras s'épuisent et que vous soyez à bout de souffle. Ce n'est que par ces exigences qu'il est possible de développer des compétences fondamentales qui aideront le praticien à faire face à la dureté de la formation.

Cependant, dans la mesure où les arts martiaux japonais (y compris le kendo) furent introduits à titre éducatif dans l'enseignement militaire ou scolaire durant l'époque moderne, les enseignants ont commencé à fixer une limite au nombre de frappes dans le kirikaeshi pour la formation des débutants. Dans _Budō Kyōhan_ (1895) de Kumamoto Jitsudō, le kirikaeshi est mentionné dans la section _« Kiso Enshū Dai-ikkyō ; Uchikomi »_. Il y a une explication selon laquelle le men de l'adversaire devrait être frappé de gauche et de droite 7 fois en avançant jusqu'à la septième frappe sur le men qui doit être à gauche (le déplacement ne se fait pas en okuri-ashi mais en ayumi-ashi, donc comme s'ils marchaient), puis on fait un grand pas en arrière, pour reculer avant le dernière frappe. Ce livre contient également des instructions détaillées sur la façon dont ukete devrait recevoir les attaques. Il est donc probable que le kirikaeshi d'aujourd'hui avec son nombre défini de frappes fut finalement établi à partir de cette méthode d'entraînement, qui a été développée en pensant aux débutants.

De son côté, Takano Sasaburō a présenté la méthode de frappes rapides alternées dans son livre

_Kendō_ . En ce qui concerne le genre de kirikaeshi qu'il pratiquait dans sa jeunesse, il écrit :

_« Kirikaeshi dans notre école (Ittō-ryū Nakanishi-ha) n'était pas la forme dans laquelle le men est frappé alternativement à gauche puis à droite. Au lieu de cela, nous frappions puissamment le même côté plusieurs fois de suite, puis nous le faisions de l’autre côté. Il n'y avait pas un nombre de frappe fixe pour chaque côté. Nous utilisions un sabre de bois sans enfiler le men. J'aurais pu faire la forme actuelle du kirikaeshi les yeux fermés. Cependant, si vous souhaitez_

_exécuter 3 frappes d'un côté, puis 4 frappes de l'autre, vous devez être très prudent afin d’éviter les blessures (Budo Hokan, 1934). »_

Cette forme de kirikaeshi utilisant un sabre de bois peut être un peu difficile à réaliser aujourd'hui, mais compte tenu de l'importance de cet exercice dont l’objectif est de

« perfectionner la technique de frappe en avant et en arrière » tout comme entraîner les pratiquants (y compris le motodachi) à être préparés mentalement à combattre « avec de vrais sabres », peut-être que ce type de méthode d'entraînement mérite d'être reconsidéré.