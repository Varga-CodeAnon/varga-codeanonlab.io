---
title: Stage à Nantes avec sensei Allan Soulas
date: 2022/03/19 13:00:00
categories: Kendo
tags:
- Évènements
top: false
---

![](/images/stage-nante2022.jpeg)

La section Kendo du **Suishinkaï** vous invite à son second stage organisé sous la direction de Monsieur **Allan Soulas**, 7ème Dan de Kendo.
Le stage aura lieu le **samedi 19 et dimanche 20 Mars** à l'espace sportif la Mainguais, Rue de la Mainguais, 44470 Carquefou

**Horaires :**
- Le samedi:- 13h-17h
- Le dimanche:-10h-12h puis 13h30-16h

Tarif unique : **7€**
Formule repas le dimanche midi : **7€**

Plus d'informations ici sur le site officieil du dojo : http://www.suishinkai-dojo.fr/calendrier/stage-de-nantes-avec-allan-soulas/