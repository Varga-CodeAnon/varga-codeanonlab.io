---
title: Senpai - Kohai
categories: ''
tags:
- Articles
top: false

---
  
_Auteur: Kenichi YOSHIMURA (texte publié en mars 1996 dans "l'écho des dojo")_

![](/images/2022-06-01_18-49.png)

Dans la pratique du Kendo, la relation humaine est un élément très important. Il est impossible de pratiquer le Kendo seul et on a toujours besoin d'un partenaire.

Si l'on se permet de frapper pleinement sur la tête de son partenaire, ce n'est pas un geste brutal et arrogant mais il s'agit de l'extériorisation réelle de son énergie sous forme de frappe au sabre.

Pour que l'attaque soit véritable et bien concentrée, il est nécessaire que l'on considère son partenaire comme l'ennemi dans un combat réel. Mais une fois que le coup a été porté, il ne doit plus subsister aucun sentiment d'affronter un ennemi.

Le partenaire redevient notre camarade; il est l'élément indispensable qui nous permet de progresser et nous devons lui garder considération et respect.

En changeant le rôle, lorsqu'on reçoit un coup en plein milieu de la tête, on doit remercier son partenaire (même si cela fait mal !), car il nous a montré là où nous étions vulnérable et comment. Ainsi on apprend où est notre faiblesse et l'on essaie de ne plus répéter la même erreur.

Si l'on arrive à s'entraîner en gardant cette notion claire à l'égard de la défaite, on peut progresser considérablement.

Par contre, ceux qui se soucient uniquement du fait d'avoir touché leur partenaire ou d'avoir été touché resteront toujours à un niveau très bas aussi bien du point de vue technique que du mental. C'est la première chose qu'il faut comprendre à l'égard du partenaire qui nous frappe.

Lorsqu'on débute en Kendo, on apprend beaucoup de choses grâce aux enseignants et aux anciens. Sans eux, on ne pourrait jamais progresser.

"Les anciens" s'appellent, en japonais, Senpai et les nouveaux arrivés Kohai. On accorde beaucoup d'importance à cette notion de relation Senpai-Kohai au Japon, non seulement dans le monde du Kendo mais aussi dans la vie.

C'est une relation de respect envers les gens plus âgés ou ceux qui ont plus d'expériences et de connaissances. Sans parler du confucianisme qui occupe toujours une grande place dans la culture extrême-orientale, c'est sûrement une manifestation de sagesse pour rendre la vie harmonieuse entre les individus.

Cette notion englobe tous les pratiquants de Kendo, au moins à l'intérieur du Japon. C'est pourquoi même des pratiquants prestigieux ou les grands champions japonais gardent, toujours avec autant de modestie, le respect envers tous les maîtres et les Senpai qui les ont guidés.

Les Senpai sont toujours mis à l'honneur par les Kohai.

Sur le plan pratique, dans le Dojo par exemple, le Kokai laisse la place " plus élevée" au Senpai. Si le Kohai demande un combat au Senpai, celui-ci se met du côté face (Shomen).

S'ils s'alignent pour le salut, l'ordre hiérarchique est respecté selon cette notion de Senpai- Kohai dans la plupart des cas. je dis " la plupart des cas", car ce n'est pas toujours la règle absolue.

Quand on doit tenir compte du grade, il n'est pas impossible que le Kohai plus gradé soit positionné à un rang plus haut que le Senpai moins gradé, lors d'un stage de caractère officiel par exemple.

Mais une fois qu'ils rentrent dans leur Dojo, ils retrouvent la hiérarchie de Senpai-Kohai.

Le grade concerne la maturité technique sans doute, mais je pense que la réelle relation humaine entre les pratiquants du Kendo devrait se construire sur cette notion de Senpai-Kohai.

Ce qu'il faut comprendre aussi, c'est que l'âge et l'ancienneté ne suffisent pas pour être un bon Senpai. Le Senpai doit être toujours un bon modèle pour le Kohai, en ayant un comportement digne. Ainsi, le respect mutuel entre le Senpai et le Kohai existera longtemps, même si le Kohai dépasse le Senpai techniquement.

La compétence technique varie suivant la capacité de chacun surtout dans le domaine du combat comme le nôtre. Il est tout à fait normal qu'il y ait des enseignants de niveaux divers.

Qu'un pratiquant dépasse son premier enseignant et qu'il en change selon son progrès, c'est souhaitable et nécessaire même. Cependant, il ne faut surtout pas qu'il oublie que chaque enseignant était, à chaque période, son Sensei et son Senpai.

Il y a forcément une différence technique entre son premier enseignant et le dernier, mais il ne doit pas exister entre eux de différence de valeur humaine.

Si l'on réussit à assimiler cette notion et à la pratiquer, et que l'on arrive à garder un sentiment de gratitude envers ses enseignants et ses Senpai, je pense que l'on pourra également être un pratiquant de Kendo modèle et que notre monde du Kendo sera toujours harmonieux.