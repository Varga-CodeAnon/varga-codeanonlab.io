---
title: Le Kendo
date: 2020-12-20 07:01:57
---

![](/images/kendo-couverture.png)

<br />

## Historique

Le **Kendo** 剣道 est la version moderne du **Kenjutsu** 剣術, escrime japonaise du temps des samouraïs.

Contrairement à d’autres arts martiaux, ce **Budo** 武道 n’est pas l’héritage technique d’un seul maître, mais de plusieurs écoles différentes appelées **Ryu** 流, abréviation de **Ryuha** 流派.

Jusqu’à l’avènement au pouvoir de Tokugawa Ieyasu en 1603, le Japon médiéval est une période de violence. Les différentes écoles dites aujourd’hui « anciennes », les **Koryu** 古流 formaient leurs guerriers pour que, lors du vrai combat, ils sortent vainqueurs.

Les entrainements étaient donc difficiles et exigeants mais codifiés. Ils se nommaient *Kata* et étaient effectués avec des sabre en bois (**bokken** 木剣 ) puis avec de vrais sabres tranchants, les **shinken** 木剣.
Cependant, les accidents arrivaient souvent.

Une école se distinguait déjà à cette époque. 
Appelée aujourd’hui **Kashima Shinden Jikishinkage Ryu** 鹿島神傳直心影流, ce koryu alors nommée à l’époque *Shinkage Ryu* en 1530, développa une nouvelle arme sous l’impulsion de son 2ème *shihanke* **Kamiizumi Ise no Kami nobutsuna** 上泉伊勢守藤原信綱 : le *fukuroshinaï*, un bambou dont la pointe est fendue et recouverte de cuir, de la pointe jusqu'à la garde, permettant une grande flexibilité.

Cette école continue son innovation avec le 7ème shihanke **Yamada Heizaemon Fujiwara no Mitsunori** 山田平左衛門光徳 surnommé *Ippusaï* 一風斎.
Il estimait qu’apprendre seulement les katas n’était pas suffisant pour rendre un guerrier le plus fort possible puisque pour lui, combat et katas n’ont rien à voir.
Il inventa donc des éléments de protection, avec un casque et de gros manchons pour les poignets.

Le 8ème shihanke, **Naganuma Shirozaemon Kunisato** 長沼四郎左衛門国郷, améliorera ces parties, initiées par son père.

C’est la naissance de l’armure de combat, le **Bogu** 防具, au début du XVIIIème siècle. 
Pour terminer sur cette école, le shinaï sera officiellement adopté en tant qu’arme d’entrainement officielle en 1855, sur décision du directeur de l’école militaire  Kobusho, le 13ème shihanke **Seiichiro Otani Nobutomo**. La garde majoritaire passera par la même occasion de *Jodan* (garde haute) à *Chudan* (garde moyenne).

Il est à noter que d’autres écoles utilisaient également des protections. C’est le cas par exemple de l’école **Ono-ha Ittō-ryū** 小野派一刀流 qui dispose également de gros gants de protections et qui utilise un shinaï.

**Sakakibara Kenkichi** 榊原鍵吉, considéré comme le père du Kendo moderne, est alors 14ème directeur de l’école *Jikishinkage* quand il créé un événement promotionnel de la pratique du shinaï et participe à des combats au shinaï contre des adversaires venant d’autres styles. Il partageait beaucoup l’idée selon laquelle chaque école avait le droit de s’imprégner des avantages techniques d’autres écoles. C’est le début du *Shinaï Geiko*, précurseur du Kendo moderne.

La période *Meiji* débuta en 1868 et avec elle, en 1876, l’interdiction de porter le sabre.
Malgré tout, bien que paraissant anachroniques et perdant la faveur du public, le *shinai geiko* survécut, entretenu par différents groupes très réfractaires aux réformes de cette nouvelle Ère.

La pratique martiale perdura et finalement, le gouvernement décidera de participer au maintien de ses traditions puisqu’en 1897, la première section d’escrime japonaise de la police de Tokyo voit le jour.

En 1895 est fondé le **Dai Nippon Butoku Kaï** 大日本武徳会, l’association pour la conservation des arts martiaux traditionnels japonais.

En 1899, le dojo du Dai Nippon Butoku Kaï, le **Butokuden** 武徳, voit le jour et, en 1900, le terme **Kendo** 剣道 est utilisé pour la première fois.

Dès le début du XXème siècle, une commission dirigée par 5 maîtres de koryu se rassemble afin d’établir les katas qui formeront la bases d’enseignement de ce nouveau Budo : le Kendo.
Ainsi se rassemblent :
    • **Negishi Shingoro** 根岸信五郎 de *Shinto Munen Ryu* 神道無念流
    • **Takano Sasaburo** 高野佐三郎 de *Ono Ha Itto Ryu* 小野派一刀流
    • **Naito Takaharu** et **Monna Tadashi** de *Hokushin Itto Ryu* 北辰一刀流兵法
    • **Tsuji Shinpei** de *Shingyoto Ryu* 心形刀流 et *Jikishinkage Ryu* 鹿島神傳直心影流 

En 1909, la pratique du Kendo est encouragée par l’Etat et la fédération japonaise voit le jour. A titre de comparaison, la fédération d’escrime française nait en 1882.
Les katas de Kendo sont officiellement définis au cours de l’année 1912, avec le *Daï Nippon Teikoku Kendo Kata* : 7 katas au grand sabre et 3 avec le petit sabre, le *kodachi*.

<br />

![Comité Dai Nippon Teikoku Kendo Kata. Negishi Shingorō est assis à l'avant à droite avec Tsuji Shinpei (Shingyoto Ryu) à côté de lui. Au fond de droite à gauche : Takano Sasaburo (Itto Ryu), Monna Tadashi (Hokushin Itto Ryu) et Naito Takaharu (Hokushin Itto Ryu). La photo date de 1912](/images/dai-nippon.png)

<br />

En 1941, le Kendo devient obligatoire à l’école quand en 1945, après la défaite lors de la seconde guerre mondiale, l’amiral américain Chester l’interdit. En effet, il voit dans les arts martiaux un élément constitutif de nationalisme et d’esprit combatif. Le *Dai Nippon Butoku Kai* est donc dissout.

Mais en 1952, il réouvre et la **Zen Nihon Ken Renmei** (ZNKR) 全日本剣道連盟 est créée.
Enfin, il faudra attendre 1970 et les premiers championnats du monde de Tokyo au Japon pour que le Kendo prenne une dimension internationale.

Si nous devions en retirer une chose, c’est que le Kendo a su parfaitement s’adapter avec ses époques.

## Le Kendo aujourd'hui

Le Kendo est à la fois un art martial, un sport et un outil de développement personnel et de cohésion sociale.

Il est pratiqué avec un shinaï et le corps est protégé par une armure.

Art martial codifié, on ne frappe pas n’importe où mais uniquement sur des parties bien précises et correspondantes à des attaques dont on crie le nom lorsque l'on porte le coup :
    • Men : attaque à la tête
    • Kote : attaque aux poignets
    • Do : attaque aux flancs
    • Tsuki : attaque à la gorge

Ces attaques sont faites en avançant et peuvent être combinées. Elles peuvent être également réalisées en reculant, sous la forme **hiki** 引き, comme *hiki-Men* par exemple.

Ces attaques font parties des **shikake-waza** 仕掛け, les attaques directes. Vous en êtes l’initiateur.

Il existe aussi les **oji-waza** 応じ, ou contre-attaques, celles que vous utilisez quand vous répondez à un assaut. Ce seront les **kaeshi waza** 返し telles que le **kaeshi-Do** par exemple, ou bien les **suriage waza** 刷り上げ ou encore les **debana waza** 出鼻.

Toutes ces techniques sont applicables en combat mais ne peuvent être portées qu’à une seule condition pour être valides :  il faut respecter le **KI KEN TAI no ICHI** 気剣体一致.

- **KI** 気 : le cri (Kiaï) émis lors de l'exécution d'un mouvement d'attaque et portant la volonté et la détermination du pratiquant
- **KEN** 剣 : utilisation du shinaï bien orienté (respect du tranchant du sabre) pour frapper une partie autorisée de l’adversaire
- **TAI** 体 : parfaite attitude du corps avec le pied droit venant frapper le sol lors de la coupe.
- **ICHI** 一致 : Synchronisation en un seul temps de ces trois notions Ki,KEN et TAI

## Enseignants

- **Jean-michel MARTINUZZI** : *Kendo 4ème Dan* et *Battodo 4ème Dan*
  Titulaire du CFEB et BFHEK
- **Jean-Pierre CABROL** : *Kendo 5ème Dan*
  Titulaire du CFEB