---
title: Crédits
date: 2020-12-20 07:01:57
---

![Logo marron](../favicon.ico)

> *Sauf oubli malheureux de ma part, tout ce qui n'est pas cité ici est par défaut sous licence [GPL-3.0](https://www.gnu.org/licenses/licenses.fr.html).*
>
> *A moins que le contraire ne soit mentionné, toutes les licences citées sont au minimum open-sources et permettent la réutilisation du contenu qui leur est associé sur vos propres blogs et autres espaces numériques personnels. J'essaye de faire un site aussi libre que possible, n'hésitez pas à en profiter.*

### Site web (front-end)
- Le code des pages HTML est généré par le moteur de blog [Hexo](https://hexo.io/) (*projet open-source sous licence [MIT](https://fr.wikipedia.org/wiki/Licence_MIT)*)
- Les fonctionnalités du site ainsi que son thème (javascript et feuilles de styles CSS) sont fournies par le thème [Ayer](https://github.com/Shen-Yu/hexo-theme-ayer) (*licence [SATA](https://github.com/zTrix/sata-license), sous-licence [MIT](https://fr.wikipedia.org/wiki/Licence_MIT)*)
- Les polices de caractères utilisées sont :
  -  [DejaVu](https://dejavu-fonts.github.io/) (*licence [Free-license](https://dejavu-fonts.github.io/License.html)*)
  -  [Liberation](https://www.redhat.com/fr/blog/liberation-fonts) (*licence [GPL](https://www.gnu.org/licenses/licenses.fr.html)*)
  -  [Ubuntu](https://design.ubuntu.com/font/) (*licence [OFL](https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL)*)
  -  [FontAwesome Free](https://fontawesome.com/icons?d=gallery&m=free) (*multiples [licences](https://fontawesome.com/license/free) open-source*)
- Les emoji et les icones (flèches de menu par exemple) sont issus de la bibliothèque open-source [Remixicon](https://github.com/Remix-Design/remixicon#usage) (*licence [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0)*)
- Le code source du site est disponnible dans son intégralité sur [Github](https://github.com/Varga-CodeAnon/Shian) sous licence [GPL-3.0](https://www.gnu.org/licenses/licenses.fr.html)

### Images
- Le logo du site visible ci-dessus en marron, mais aussi en blanc sur la page d'accueil et sous forme d'icone dans la barre de recherche est sous licence [GPL-3.0](https://www.gnu.org/licenses/licenses.fr.html)
- *[A Break for Devotion](https://unsplash.com/photos/MZ00oma1sn0)*, photo prise par Ismael Abelleira ([@ismaelab](https://unsplash.com/@ismaelab)) (*sous licence [Unsplash](https://unsplash.com/license)*)	
- *[Gorintō](https://search.creativecommons.org/photos/2a322498-3031-4ecc-98ee-147432395966)*, photo prise par Karolina Lubryczynska ([@karolajnat](https://www.flickr.com/photos/9532859@N02)) (*sous licence [CC BY-NC-ND 2.0](https://creativecommons.org/licenses/by-nc-nd/2.0/?ref=ccsearch&atype=rich)*)
- *[The light that never goes out!](https://unsplash.com/photos/UiMkBvDQSAA)*, photo prise par Hans Vivek ([@oneshotespresso](https://unsplash.com/@oneshotespresso)) (*sous licence [Unsplash](https://unsplash.com/license)*)	

### Articles
