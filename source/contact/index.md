---
title: Contact
date: 2020-12-20 10:01:57
---

<p>N’hésitez pas à nous suivre sur les réseaux !</p>
<ul>
<li><a href="https://www.facebook.com/chowakan"><img src="/images/fb.png" style="width: 25px; float:left"></a>&ensp;Chowakan kendo</li>
<li><a href="https://www.instagram.com/chowakan_kendo"><img src="/images/insta.png" style="width: 25px; float:left"></a>&ensp;chowakan_kendo</li>
</ul>
<p>Vous pouvez aussi nous contacter par mail pour tout renseignement à l’adresse <a href="mailto: chowakan.dojo@gmail.com">chowakan.dojo@gmail.com</a>, ou via le formulaire de contact ci-dessous</p>
<hr>

<style>
  body {font-family: Arial, Helvetica, sans-serif;}
  * {box-sizing: border-box;}
  
  input[type=text], select, textarea {
    width: 100%;
    padding: 12px;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
    margin-top: 6px;
    margin-bottom: 16px;
    resize: vertical;
  }
  
  input[type=submit] {
    background-color: #04AA6D;
    color: white;
    padding: 12px 20px;
    border: none;
    border-radius: 4px;
    cursor: pointer;
  }
  
  input[type=submit]:hover {
    background-color: #45a049;
  }
  
  .container {
    border-radius: 5px;
    background-color: #f2f2f2;
    padding: 20px;
  }
  </style>
  
  <div class="container">
    <form action="https://getform.io/f/6f10f1e0-03c4-48a3-bfde-a568340924e8" method="POST">
      <label for="name">Nom</label>
      <input type="text" id="fname" name="firstname" placeholder="Votre nom...">
      <label for="mail">Adresse mail</label>
      <input type="text" id="lname" name="lastname" placeholder="Votre adresse mail...">  
      <label for="subject">Message</label>
      <textarea id="subject" name="subject" placeholder="Votre message.." style="height:200px"></textarea>  
      <input type="submit" value="Envoyer">
    </form>
  </div>